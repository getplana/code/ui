# Snyk Profiles

- [react-error-boundary](https://snyk.io/advisor/npm-package/react-error-boundary)
- [react-textarea-autosize](https://snyk.io/advisor/npm-package/react-textarea-autosize)
- [use-debounce](https://snyk.io/advisor/npm-package/use-debounce)