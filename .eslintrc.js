module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      impliedStrict: true,
      jsx: true,
    },

    // Needed for @typescript-eslint/recommended-requiring-type-checking config.
    // Source: https://github.com/typescript-eslint/typescript-eslint/blob/master/docs/getting-started/linting/TYPED_LINTING.md
    tsconfigRootDir: __dirname,
    project: ['./tsconfig.json'],
  },
  plugins: ['@typescript-eslint', 'react'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:import/recommended',
    'plugin:import/typescript',
    'plugin:jsx-a11y/strict',
    'react-app',
    'airbnb-typescript',
  ],
  rules: {
    // --- Disable rules --- //

    // Disable since no longer needed with React v17.
    // Source: https://github.com/yannickcr/eslint-plugin-react/issues/2928
    'react/react-in-jsx-scope': 'off',

    // Disable since defaultProps will eventually be deprecated.
    // Source: https://react-typescript-cheatsheet.netlify.app/docs/basic/getting-started/default_props/
    'react/require-default-props': 'off',

    // Disable to avoid having to define return types of functional and class components, which
    // isn't really needed.
    // Source: https://github.com/typescript-cheatsheets/react/issues/129#issuecomment-508412266
    '@typescript-eslint/explicit-module-boundary-types': 'off',

    // --- Override airbnb-typescript rules --- //

    // Enable allowAsStatement in order for ignoreVoid option of
    // @typescript-eslint/no-floating-promises rule to work.
    // Source: https://github.com/typescript-eslint/typescript-eslint/blob/v4.20.0/packages/eslint-plugin/docs/rules/no-floating-promises.md#ignorevoid
    'no-void': ['error', {
      allowAsStatement: true,
    }],

    'object-curly-newline': ['error', {
      ObjectExpression: { minProperties: 2, multiline: true, consistent: true },
      ObjectPattern: { minProperties: 2, multiline: true, consistent: true },
      ImportDeclaration: { minProperties: 2, multiline: true, consistent: true },
      ExportDeclaration: { minProperties: 2, multiline: true, consistent: true },
    }],

    'import/order': ['error', {
      alphabetize: {
        order: 'asc',
      },
      groups: ['external', 'parent', 'sibling'],
      'newlines-between': 'always',
    }],

    'react/jsx-props-no-multi-spaces': 'off',

    'react/sort-prop-types': ['error', {
      callbacksLast: true,
    }],

    // --- Add rules --- //

    // Enable ignoreVoid to allow promises consumed with void operator, which is used for promises
    // in React's useEffect hook.
    // Source: https://github.com/typescript-eslint/typescript-eslint/issues/1184
    '@typescript-eslint/no-floating-promises': ['error', {
      ignoreVoid: true,
    }],
  },
};
