import { Flex } from '@chakra-ui/layout';
import {
  Button,
  Icon,
  IconButton,
  Text,
} from '@chakra-ui/react';
import { DraggableSyntheticListeners } from '@dnd-kit/core';
import {
  SortableContext,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import {
  memo,
  MouseEvent as ReactMouseEvent,
  useState,
} from 'react';

import { ReactComponent as DragHandleIcon } from '../../assets/drag.svg';
import { ReactComponent as EditIcon } from '../../assets/edit.svg';
import { ReactComponent as EllipsisIcon } from '../../assets/ellipsis.svg';
import { ReactComponent as TrashIcon } from '../../assets/trash.svg';
import * as constants from '../../constants';
import useMenu from '../../hooks/use-menu';
import {
  TaskGroupInputMode,
  TaskModel,
} from '../../types';
import ActionMenuItem, { Props as ActionMenuItemProps } from '../action-menu-item';
import Menu from '../menu';
import SortableTaskItem from '../sortable-task-item';
import TaskDialog from '../task-dialog';
import TaskGroupInput from '../task-group-input';

const moreButtonMarginLeft = 1.2;

export type TaskGroupProps = {
  id: number;
  name: string;
  tasks: TaskModel[];
};

interface Props extends TaskGroupProps {
  listeners: DraggableSyntheticListeners;
  isDragging: boolean;
}

function TaskGroup(props: Props) {
  const {
    id,
    name,
    tasks,
    isDragging,
    listeners,
  } = props;

  const [isSelected, setIsSelected] = useState(false);
  const [isHoveringTitle, setIsHoveringTitle] = useState(false);
  const [isRenaming, setIsRenaming] = useState(false);
  const [isTaskDialogOpen, setIsTaskDialogOpen] = useState(false);
  const actionMenu = useMenu();

  const handleMoreButtonClick = (event: ReactMouseEvent<HTMLElement>) => {
    actionMenu.openAtElement(event.currentTarget);
    setIsSelected(true);
  };

  const handleRenameClick = () => {
    actionMenu.close();
    setIsRenaming(true);
    setIsSelected(false);
  };

  const handleMenuClose = () => {
    actionMenu.close();
    setIsSelected(false);
  };

  const groupActions = [
    {
      text: 'Rename',
      leftIcon: EditIcon,
      onClick: handleRenameClick,
    },
    {
      text: 'Delete',
      leftIcon: TrashIcon,
    },
  ] as ActionMenuItemProps[];

  return (
    <>
      <Flex
        direction="column"
        marginBottom={`${constants.TASK_GROUP.MARGIN_VERTICAL}rem`}
        visibility={isDragging ? 'hidden' : undefined}
      >
        <Flex
          direction="row"
          alignItems="center"
          onMouseOver={() => setIsHoveringTitle(true)}
          onMouseLeave={() => setIsHoveringTitle(false)}
        >
          <Flex
            padding={`${constants.DRAG_HANDLE.PADDING}rem`}
            marginRight={`${constants.DRAG_HANDLE.MARGIN_RIGHT}rem`}
            borderRadius="0.4rem"
            cursor="move"
            _hover={{ background: constants.GRAY_150 }}

            // eslint-disable-next-line react/jsx-props-no-spreading
            {...listeners}
          >
            <Icon
              as={DragHandleIcon}
              color={constants.GRAY_800}
              width={`${constants.DRAG_HANDLE.ICON_WIDTH}rem`}
              height="1.4rem"
              visibility={isHoveringTitle && !isRenaming ? 'visible' : 'hidden'}
            />
          </Flex>
          {isRenaming ? (
            <TaskGroupInput
              mode={TaskGroupInputMode.Renaming}
              isOpen={isRenaming}
              groupID={id}
              groupName={name}
              onClose={() => setIsRenaming(false)}
            />
          ) : (
            <Text
              isTruncated
              color={constants.GRAY_1000}
              fontSize="1.6rem"
              onClick={() => setIsRenaming(true)}

              // 1px left padding to match 1px border for task group input.
              padding={`${constants.TASK_GROUP.OVERLAY_PADDING}rem 0.1rem`}
            >
              {name}
            </Text>
          )}
          <IconButton
            aria-label="Task group actions"
            icon={(<Icon as={EllipsisIcon} boxSize="1.6rem" />)}
            color={constants.GRAY_700}
            minWidth="2.4rem"
            height="2.4rem"
            borderRadius="0.4rem"
            background="none"
            marginLeft={`${moreButtonMarginLeft}rem`}
            display={isRenaming ? 'none' : undefined}
            visibility={isHoveringTitle || isSelected ? 'visible' : 'hidden'}
            transition="visibility 0s"
            className={`${constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}`}
            _hover={{
              color: constants.GRAY_900,
              background: constants.GRAY_150,
            }}
            onClick={handleMoreButtonClick}
          />
        </Flex>
        <Flex
          direction="column"
          marginTop={tasks.length !== 0 ? `${constants.TASK_GROUP.MARGIN_VERTICAL}rem` : '0.8rem'}
          marginLeft={`${constants.DRAG_HANDLE.WIDTH + constants.DRAG_HANDLE.MARGIN_RIGHT}rem`}
        >
          <SortableContext
            items={tasks.map((task) => `task-${task.id}`)}
            strategy={verticalListSortingStrategy}
          >
            {tasks.map((task) => (
              <SortableTaskItem
                key={task.id}
                task={task}
              />
            ))}
          </SortableContext>
        </Flex>
        <Button
          alignSelf="flex-start"
          color={constants.GRAY_400}
          fontSize="1.4rem"
          height="2.8rem"
          padding="0rem 0.8rem"
          background="white"
          marginTop="0.8rem"
          marginLeft={`${constants.DRAG_HANDLE.WIDTH
            + constants.DRAG_HANDLE.MARGIN_RIGHT
            + constants.DRAG_HANDLE.WIDTH}rem`}
          display={isTaskDialogOpen ? 'none' : undefined}
          className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
          _hover={{
            color: constants.GRAY_700,
            background: constants.GRAY_125,
          }}
          onClick={() => setIsTaskDialogOpen(true)}
        >
          + Add task
        </Button>
        <TaskDialog
          isOpen={isTaskDialogOpen}
          groupID={id}
          onClose={() => setIsTaskDialogOpen(false)}
        />
      </Flex>
      <Menu
        open={actionMenu.anchorPosition !== null || actionMenu.anchorElement !== null}
        anchorReference={actionMenu.anchorType}
        anchorEl={actionMenu.anchorElement}
        anchorPosition={actionMenu.anchorPosition ?? undefined}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: actionMenu.anchorType === constants.ANCHOR_TYPE_ELEMENT ? 'center' : 'left',
        }}
        onClose={handleMenuClose}
      >
        {groupActions.map((action, index) => (
          <ActionMenuItem
            text={action.text}
            isSelected={action.isSelected}
            leftIcon={action.leftIcon}
            rightIcon={action.rightIcon}
            onMouseEnter={action.onMouseEnter}
            onMouseLeave={action.onMouseLeave}
            onClick={action.onClick}

            // eslint-disable-next-line react/no-array-index-key
            key={index}
          />
        ))}
      </Menu>
    </>
  );
}

export default memo(TaskGroup);
