import {
  Flex,
  HStack,
  Text,
  VStack,
} from '@chakra-ui/layout';

import * as constants from '../../constants';
import { GoalsProvider } from '../../context/goals';
import { TasksProvider } from '../../context/tasks';
import useLocalStorage from '../../hooks/use-local-storage';
import GoalsPanel from '../goals-panel';
import GoalsPanelButton from '../goals-panel-button';
import TaskGroupList from '../task-group-list';

const customerID = 1;
const headerFontSize = 3.0;
const today = new Date();
const todayStr = today.toLocaleDateString('en-US', {
  weekday: 'long',
  month: 'long',
  day: 'numeric',
});

function TaskPage() {
  const [isPanelOpen, setIsPanelOpen] = useLocalStorage('isGoalPanelOpen', false);

  return (
    <GoalsProvider
      customerID={customerID}
    >
      <TasksProvider
        customerID={customerID}
      >
        <HStack
          flex={1}
          spacing="0rem"

          // Defaults to "center" for HStack
          alignItems="stretch"
        >
          <Flex
            flexDirection="column"
            flex={1}
            alignItems="center"

            // For absolute position for goals panel button.
            position="relative"
          >
            <Flex
              width={`${constants.TASK_GROUP.WIDTH}rem`}
              marginTop="4.0rem"
              marginBottom="3.2rem"
            >
              <VStack
                marginLeft={`${constants.DRAG_HANDLE.WIDTH + constants.DRAG_HANDLE.MARGIN_RIGHT}rem`}
                align="left"
                spacing="0.4rem"
              >
                <Text
                  color={constants.GRAY_1000}
                  fontSize={`${headerFontSize}rem`}
                  fontWeight="bold"
                  lineHeight={`${headerFontSize}rem`}
                >
                  Today
                </Text>
                <Text
                  color={constants.GRAY_800}
                  fontSize="1.4rem"
                  lineHeight="1.4rem"

                  // Date for some reason looks more left than header.
                  marginLeft="0.2rem"
                >
                  {todayStr}
                </Text>
              </VStack>
            </Flex>
            <Flex
              flexDirection="column"
              alignSelf="stretch"
              flex={1}
              alignItems="center"
              overflowY="auto"

              // For offsetTop for task dialog container.
              // Source: https://stackoverflow.com/questions/17974859/difference-between-offsetparent-and-parentelement-or-parentnode
              position="relative"
            >
              <TaskGroupList />
            </Flex>
            <GoalsPanelButton
              isVisible={!isPanelOpen}
              onClick={() => setIsPanelOpen(true)}
              top="3.6rem"
            />
          </Flex>
          <GoalsPanel
            isOpen={isPanelOpen}
            onClose={() => setIsPanelOpen(false)}
          />
        </HStack>
      </TasksProvider>
    </GoalsProvider>
  );
}

export default TaskPage;
