import { Flex } from '@chakra-ui/layout';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

import * as constants from '../../constants';
import TaskGroup, { TaskGroupProps } from '../task-group';

// Source: https://docs.dndkit.com/api-documentation/draggable/drag-overlay#presentational-components
function SortableTaskGroup(props: TaskGroupProps) {
  const { id } = props;

  const {
    setNodeRef,
    listeners,
    transform,
    transition,
    isDragging,
  } = useSortable({
    id: `group-${id}`,
    data: {
      type: constants.TASK_GROUP_TYPE,
    },
  });

  return (
    <Flex
      ref={setNodeRef}
      transform={transform ? CSS.Translate.toString(transform) : undefined}
      transition={transition}
      justifyContent="center"
    >
      <TaskGroup
        listeners={listeners}
        isDragging={isDragging}

        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
      />
    </Flex>
  );
}

export default SortableTaskGroup;
