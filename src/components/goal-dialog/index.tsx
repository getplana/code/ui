import { VStack } from '@chakra-ui/layout';
import { Text } from '@chakra-ui/react';
import {
  ChangeEvent,
  useState,
} from 'react';

import * as constants from '../../constants';
import { useColors } from '../../context/colors';
import { useGoals } from '../../context/goals';
import { ColorModel } from '../../types';
import getHslString from '../../utils/get-hsl-string';
import ColorRadioGroup from '../color-radio-group';
import Dialog from '../dialog';
import Textarea from '../textarea';

function initNameToColor(colors: ColorModel[]): Map<string, ColorModel> {
  const nameToColor: Map<string, ColorModel> = new Map<string, ColorModel>();
  colors.forEach((color) => {
    nameToColor.set(color.name, color);
  });

  return nameToColor;
}

type Props = {
  isOpen: boolean;
  onClose: () => void;
};

function GoalDialog(props: Props) {
  const {
    isOpen,
    onClose: handleClose,
  } = props;

  const [goalText, setGoalText] = useState('');
  const [selectedColor, setSelectedColor] = useState<ColorModel | null>(null);
  const { addGoal } = useGoals(true);

  const colors = useColors();
  const nameToColor = initNameToColor(colors);

  const resetState = () => {
    setGoalText('');
    setSelectedColor(null);
  };

  const handleAddClick = async () => {
    if (goalText !== '' && selectedColor !== null) {
      handleClose();

      await addGoal(goalText, selectedColor.id);

      resetState();
    }
  };

  const handleDialogClose = () => {
    handleClose();
    resetState();
  };

  return (
    <Dialog
      isOpen={isOpen}
      isForm
      isDestructive={false}
      closeOnOverlayClick={false}
      title="Add goal"
      primary={{
        text: 'Add',
        isDisabled: goalText === '' || selectedColor === null,
        onClick: handleAddClick,
      }}
      secondary={{
        text: 'Cancel',
        onClick: handleDialogClose,
      }}
      onClose={handleDialogClose}
    >
      <VStack
        alignItems="flex-start"
        spacing="0.8rem"
      >
        <Text
          color={constants.GRAY_600}
          fontWeight="bold"
        >
          Name
        </Text>
        <Textarea
          value={goalText}
          color={selectedColor ? getHslString(selectedColor) : 'black'}
          minRows={2}
          background={constants.GRAY_150}
          padding="0.8rem 1.2rem"
          onChange={(event: ChangeEvent<HTMLTextAreaElement>) => {
            setGoalText(event.currentTarget.value);
          }}
        />
      </VStack>
      <VStack
        alignItems="flex-start"
        spacing="0.8rem"
        marginTop="2.4rem"
      >
        <Text
          color={constants.GRAY_600}
          fontWeight="bold"
        >
          Color
        </Text>
        <ColorRadioGroup
          nameToColor={nameToColor}
          onChange={(name) => setSelectedColor(nameToColor.get(name) ?? null)}
        />
      </VStack>
    </Dialog>
  );
}

export default GoalDialog;
