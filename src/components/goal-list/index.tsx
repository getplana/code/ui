import { VStack } from '@chakra-ui/layout';
import {
  MouseEvent,
  useCallback,
  useState,
} from 'react';
import {
  DragDropContext,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd';
import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group';
import { useDebouncedCallback } from 'use-debounce';

import { ReactComponent as ArchiveIcon } from '../../assets/archive.svg';
import { ReactComponent as RightChevronIcon } from '../../assets/chevron-right.svg';
import { ReactComponent as ColorIcon } from '../../assets/color.svg';
import { ReactComponent as EditIcon } from '../../assets/edit.svg';
import { ReactComponent as TrashIcon } from '../../assets/trash.svg';
import * as constants from '../../constants';
import { useColors } from '../../context/colors';
import { useGoals } from '../../context/goals';
import { useTasks } from '../../context/tasks';
import useMenu from '../../hooks/use-menu';
import {
  DataSource,
  GoalModel,
  HSLModel,
  LogLevel,
} from '../../types';
import areColorsEqual from '../../utils/are-colors-equal';
import log from '../../utils/log';
import ActionMenuItem, { Props as ActionMenuItemProps } from '../action-menu-item';
import ColorMenuItem from '../color-menu-item';
import Dialog from '../dialog';
import GoalItem from '../goal-item';
import Menu from '../menu';
import NestedMenu from '../nested-menu';
import TextareaDialog from '../textarea-dialog';

import './style.scss';

type Props = {
  isActive: boolean;
  width: number;
};

function GoalList(props: Props) {
  const {
    isActive,
    width,
  } = props;

  const [selectedGoal, setSelectedGoal] = useState<GoalModel | null>(null);
  const [isTextareaDialogOpen, setIsTextareaDialogOpen] = useState(false);
  const [textareaDialogAnchor, setTextareaDialogAnchor] = useState<HTMLElement | null>(null);
  const [textareaDialogText, setTextareaDialogText] = useState('');
  const [colorMenuAnchor, setColorMenuAnchor] = useState<HTMLElement | null>(null);
  const [isAlertDialogOpen, setIsAlertDialogOpen] = useState(false);
  const actionMenu = useMenu();
  const colors = useColors();
  const {
    goals,
    updateGoal,
    updateGoalText,
    reorderGoal,
    deleteGoal,
  } = useGoals(isActive);
  const {
    updateTaskColorsClient,
    deleteGoalTasksClient,
  } = useTasks();

  // --- Helpers --- //

  const closeActionMenu = () => {
    actionMenu.close();
    setSelectedGoal(null);
    setTextareaDialogAnchor(null);
    setTextareaDialogText('');
  };

  // --- Handlers: Goal Items --- //

  const handleGoalRightClick = useCallback((event: MouseEvent<HTMLElement>, goal: GoalModel) => {
    event.preventDefault();

    const position = {
      left: event.pageX,
      top: event.pageY,
    };

    // Highlight selected goal.
    setSelectedGoal(goal);

    // Open action menu.
    setTextareaDialogAnchor(event.currentTarget);
    actionMenu.openAtPosition(position);

    // Save text of selected goal.
    setTextareaDialogText(goal.text);
  }, [actionMenu]);

  const handleMoreButtonClick = useCallback(async (
    event: MouseEvent<HTMLElement>,
    goal: GoalModel,
  ) => {
    if (event.currentTarget.parentElement === null) {
      await log(LogLevel.Error, 'Parent centering div for more button of goal item is null, so could not open goal menu on clicking more button.');
      return;
    }

    actionMenu.openAtElement(event.currentTarget);
    setSelectedGoal(goal);
    setTextareaDialogText(goal.text);

    // Parent is centering div for goal item's more button, whereas parent of the centering div is
    // the goal item.
    setTextareaDialogAnchor(event.currentTarget.parentElement.parentElement);
  }, [actionMenu]);

  // --- Handlers: Drag and Drop --- //

  const handleDragStart = () => {
    // Set pointer events to none to prevent triggering hover events when dragging task.
    // Source: https://github.com/atlassian/react-beautiful-dnd/blob/master/docs/guides/preset-styles.md#phase-dragging-droppable-element
    document.body.style.pointerEvents = 'none';
  };

  const handleDragEnd = async (result: DropResult) => {
    // Reset pointer events.
    document.body.style.pointerEvents = 'auto';

    const {
      source,
      destination,
    } = result;

    // If the task is dropped outside of the list.
    if (destination === undefined) {
      return;
    }

    const sourceIndex = source.index;
    const destinationIndex = destination.index;

    // If the task is moved back to its original location.
    if (sourceIndex === destinationIndex) {
      return;
    }

    await reorderGoal(sourceIndex, destinationIndex);
  };

  // --- Handlers: Action Menu --- //

  const handleChangeColorHover = (event: MouseEvent<HTMLElement>) => {
    // Prevent color menu from opening if user quickly hovers to "Change color" after clicking
    // "Rename" and opening textarea dialog.
    if (!isTextareaDialogOpen) {
      setColorMenuAnchor(event.currentTarget);
    }
  };

  const handleArchiveUnarchiveClick = async () => {
    if (selectedGoal !== null) {
      const selectedGoalID = selectedGoal.id;
      closeActionMenu();

      await updateGoal(selectedGoalID, {
        isActive: !isActive,
      });
    }
  };

  // --- Handlers: Textarea Dialog --- //

  const debounce = useDebouncedCallback(async (debouncedText: string, selectedGoalID: number) => {
    await updateGoalText(selectedGoalID, debouncedText, DataSource.Server);
  }, 500);

  const handleTextareaDialogChange = async (newText: string) => {
    if (selectedGoal !== null) {
      setTextareaDialogText(newText);
      await updateGoalText(selectedGoal.id, newText, DataSource.Client);
      await debounce(newText, selectedGoal.id);
    }
  };

  const handleTextareaDialogClose = async () => {
    setIsTextareaDialogOpen(false);
    setTextareaDialogAnchor(null);
    setSelectedGoal(null);
    setTextareaDialogText('');
  };

  // --- Handlers: Color Menu --- //

  const handleColorClick = async (colorID: number, colorHSL: HSLModel) => {
    if (selectedGoal !== null) {
      const selectedGoalID = selectedGoal.id;
      setColorMenuAnchor(null);
      closeActionMenu();

      updateTaskColorsClient(selectedGoalID, colorHSL);
      await updateGoal(selectedGoalID, {
        colorID,
        colorHSL,
      });
    }
  };

  // --- Handlers: Alert Dialog --- //

  const handleDeleteGoal = async () => {
    if (selectedGoal !== null) {
      const selectedGoalID = selectedGoal.id;
      setIsAlertDialogOpen(false);
      closeActionMenu();

      deleteGoalTasksClient(selectedGoalID);
      await deleteGoal(selectedGoalID);
    }
  };

  const handleDialogClose = () => {
    setIsAlertDialogOpen(false);
    closeActionMenu();
  };

  // --- Goal Actions --- //

  const changeColorText = 'Change color';
  const goalAction = [
    {
      text: 'Rename',
      leftIcon: EditIcon,
      onClick: () => {
        actionMenu.close();
        setIsTextareaDialogOpen(true);
      },
    },
    {
      text: changeColorText,
      isSelected: colorMenuAnchor !== null,
      leftIcon: ColorIcon,
      rightIcon: RightChevronIcon,
      onMouseEnter: handleChangeColorHover,
      onMouseLeave: () => setColorMenuAnchor(null),
    },
    {
      text: isActive ? 'Archive' : 'Unarchive',
      leftIcon: ArchiveIcon,
      onClick: handleArchiveUnarchiveClick,
    },
    {
      text: 'Delete',
      leftIcon: TrashIcon,
      onClick: () => setIsAlertDialogOpen(true),
    },
  ] as ActionMenuItemProps[];

  return (
    <>
      <DragDropContext
        onDragStart={handleDragStart}
        onDragEnd={handleDragEnd}
      >
        <Droppable droppableId="goals-list">
          {(provided) => (
            <VStack
              spacing="0rem"

              // Add spacing between last goal and bottom of viewport.
              marginBottom="2.4rem"

              /* eslint-disable @typescript-eslint/unbound-method, react/jsx-props-no-spreading */
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              <TransitionGroup
                // Prevents TransitionGroup from rendering anything in DOM.
                // Source: http://reactcommunity.org/react-transition-group/transition-group#TransitionGroup-prop-component.
                component={null}
              >
                {goals?.map((goal, index) => (
                  <CSSTransition
                    key={goal.id}
                    classNames="goal"
                    timeout={300}
                  >
                    <GoalItem
                      goal={goal}
                      isNumbered={isActive}
                      isSelected={selectedGoal?.id ? goal.id === selectedGoal?.id : false}
                      panelWidth={width}
                      draggableIndex={index}
                      onMoreButtonClick={handleMoreButtonClick}
                      onContextMenu={handleGoalRightClick}
                    />
                  </CSSTransition>
                ))}
              </TransitionGroup>
              {provided.placeholder}
            </VStack>
          )}
        </Droppable>
      </DragDropContext>
      <Menu
        open={actionMenu.anchorPosition !== null || actionMenu.anchorElement !== null}
        anchorReference={actionMenu.anchorType}
        anchorEl={actionMenu.anchorElement}
        anchorPosition={actionMenu.anchorPosition ?? undefined}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: actionMenu.anchorType === constants.ANCHOR_TYPE_ELEMENT ? 'right' : 'left',
        }}
        onClose={() => {
          actionMenu.close();
          setSelectedGoal(null);
        }}
      >
        {goalAction.map((action, index) => (
          <ActionMenuItem
            text={action.text}
            isSelected={action.isSelected}
            leftIcon={action.leftIcon}
            rightIcon={action.rightIcon}
            onClick={action.onClick}
            onMouseEnter={action.onMouseEnter}
            onMouseLeave={action.onMouseLeave}

            // eslint-disable-next-line react/no-array-index-key
            key={index}
          >
            {action.text === changeColorText && (
              <NestedMenu
                isNotScrollable
                open={colorMenuAnchor !== null}
                anchorEl={colorMenuAnchor}
              >
                {colors.map((color) => {
                  const colorHSL = {
                    hue: color.hue,
                    saturation: color.saturation,
                    lightness: color.lightness,
                  };

                  return (
                    <ColorMenuItem
                      key={color.id}
                      text={color.name}
                      color={colorHSL}
                      isSelected={selectedGoal
                        ? areColorsEqual(selectedGoal.color, colorHSL)
                        : false}
                      onClick={() => handleColorClick(color.id, colorHSL)}
                    />
                  );
                })}
              </NestedMenu>
            )}
          </ActionMenuItem>
        ))}
      </Menu>
      <TextareaDialog
        open={isTextareaDialogOpen}
        anchorElement={textareaDialogAnchor}
        text={textareaDialogText}
        color={selectedGoal?.color ?? null}
        onChange={handleTextareaDialogChange}
        onClose={handleTextareaDialogClose}
      />
      <Dialog
        isOpen={isAlertDialogOpen}
        isForm={false}
        isDestructive
        title="Delete goal?"
        primary={{
          text: 'Delete',
          onClick: handleDeleteGoal,
        }}
        secondary={{
          text: 'Cancel',
          onClick: handleDialogClose,
        }}
        onClose={handleDialogClose}
      >
        <p>
          This will also delete
          {' '}
          <strong><em>all tasks</em></strong>
          {' '}
          in Today and Tomorrow that are assigned to this goal.
        </p>
      </Dialog>
    </>
  );
}

export default GoalList;
