import { Box } from '@chakra-ui/react';

import * as constants from '../../constants';
import AvatarButton from '../avatar-button';
import NavList from '../nav-list';

function NavBar() {
  return (
    <Box
      width="25.6rem"
      background={constants.GRAY_100}
      borderRightColor={constants.GRAY_200}
      borderRightStyle="solid"
      borderRightWidth="0.1rem"
    >
      <Box
        padding="2rem 0 2rem 4rem"
        width="100%"
      >
        <AvatarButton customerName="Shawn" />
      </Box>
      <NavList />
    </Box>
  );
}

export default NavBar;
