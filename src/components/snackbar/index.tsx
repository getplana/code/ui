import {
  Divider,
  Flex,
  Icon,
  IconButton,
} from '@chakra-ui/react';
import { ReactNode } from 'react';

import { ReactComponent as XIcon } from '../../assets/x.svg';
import * as constants from '../../constants';

type Props = {
  onClose: () => void;
  children: ReactNode;
};

function Snackbar(props: Props) {
  const {
    onClose: handleClose,
    children,
  } = props;

  return (
    <Flex
      color="white"
      background={constants.GRAY_1000}
      width="32.0rem"
      height="4.0rem"
      paddingLeft="1.6rem"
      paddingRight="0.8rem"
      borderRadius="0.4rem"
      alignItems="center"
    >
      <Flex
        grow={1}
        paddingRight="0.8rem"
        alignItems="center"
      >
        {children}
      </Flex>
      <Divider
        orientation="vertical"
        height="2.4rem"
      />
      <IconButton
        _hover={{ background: constants.GRAY_800 }}
        icon={(<Icon as={XIcon} boxSize="0.8rem" />)}
        aria-label="Close message"
        className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
        background="none"
        borderRadius="0.4rem"
        marginLeft="0.7rem"
        onClick={handleClose}
      />
    </Flex>
  );
}

export default Snackbar;
