import { Flex } from '@chakra-ui/layout';
import {
  Button,
  Text,
} from '@chakra-ui/react';
import {
  closestCenter,
  CollisionDetection,
  defaultDropAnimationSideEffects,
  DndContext,
  DragEndEvent,
  DragOverEvent,
  DragOverlay,
  DragStartEvent,
  getFirstCollision,
  MeasuringStrategy,
  MouseSensor,
  rectIntersection,
  UniqueIdentifier,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  SortableContext,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { Backdrop } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import {
  useRef,
  useState,
} from 'react';
import { createPortal } from 'react-dom';
import { Virtuoso } from 'react-virtuoso';

import * as constants from '../../constants';
import { useTasks } from '../../context/tasks';
import {
  LogLevel,
  TaskGroupDragOverlay,
  TaskGroupInputMode,
  TaskModel,
} from '../../types';
import getGroupFromID from '../../utils/drag-and-drop/get-group-from-id';
import getTaskFromID from '../../utils/drag-and-drop/get-task-from-id';
import log from '../../utils/log';
import SortableTaskGroup from '../sortable-task-group';
import TaskGroupInput from '../task-group-input';
import TaskItem from '../task-item';

import './style.scss';

const addGroupMarginTop = 0.8;
const addGroupButtonPadding = 0.8;
const scrollMarginBottom = 32;
const groupDragOverlay = {
  lineHeight: 2.4,
  border: 0.1,
};
Object.freeze(groupDragOverlay);

// Minus add group button padding to align text instead background of button
// with group titles.
const addGroupMarginLeft = constants.DRAG_HANDLE.WIDTH
  + constants.DRAG_HANDLE.MARGIN_RIGHT
  - addGroupButtonPadding;

const StyledBackdrop = withStyles({
  root: {
    // NOTE: I tried setting height for backdrop using useRef() and withStyles()/makeStyles(), but
    // it wasn’t working.
    height: `${groupDragOverlay.lineHeight
      + (constants.TASK_GROUP.OVERLAY_PADDING * 2)
      + groupDragOverlay.border}rem`,
  },
})(Backdrop);

function TaskGroupList() {
  const [draggedTask, setDraggedTask] = useState<TaskModel | null>(null);
  const [draggedGroup, setDraggedGroup] = useState<TaskGroupDragOverlay | null>(null);
  const [isTaskGroupInputOpen, setIsTaskGroupInputOpen] = useState(false);
  const [draggableType, setDraggableType] = useState<string | null>(null);
  const lastTaskOverID = useRef<UniqueIdentifier | null>(null);
  const {
    taskGroups,
    moveTaskToNewGroup,
    moveTaskToNewOrder,
    reorderTaskGroup,
  } = useTasks();
  const sensors = useSensors(useSensor(MouseSensor));

  // --- Handlers: DndContext --- //

  const handleCollisionDetection: CollisionDetection = (args) => {
    if (draggableType === constants.TASK_GROUP_TYPE) {
      return closestCenter({
        ...args,
        droppableContainers: args.droppableContainers.filter(
          // NOTE: Cannot log if ref for container is null since filter cannot take async function,
          // else reordering for groups does not work.
          (container) => container.data.current?.type === constants.TASK_GROUP_TYPE,
        ),
      });
    }

    if (draggableType === constants.TASK_TYPE) {
      const rectIntersections = rectIntersection(args);
      const overID = getFirstCollision(rectIntersections, 'id');
      if (overID !== null) {
        lastTaskOverID.current = overID;
        return [{ id: overID }];
      }
    }

    return lastTaskOverID.current ? [{ id: lastTaskOverID.current }] : [];
  };

  const handleDragStart = async ({ active }: DragStartEvent) => {
    const { data } = active;

    if (data.current === undefined) {
      await log(LogLevel.Error, 'Ref for dragged task/group is undefined on drag start, so could not reorder task/group.');
      return;
    }

    if (data.current.type === undefined) {
      await log(LogLevel.Error, 'Dragged task/group is missing type property on drag start, so could not reorder task/group.');
      return;
    }

    const activeID = Number((active.id as string).split('-')[1]);

    /**
     * Save info of dragged element on drag start since can be unmounted when scrolled out of
     * virtual window.
     */

    const newDraggableType = data.current.type as string;
    setDraggableType(newDraggableType);

    if (newDraggableType === constants.TASK_TYPE) {
      const task = getTaskFromID(taskGroups, activeID);
      if (task === undefined) {
        await log(LogLevel.Error, `Dragged task ${activeID} not found in task group list on drag start, so could not reorder dragged task.`);
        return;
      }

      setDraggedTask(task);
    }

    if (newDraggableType === constants.TASK_GROUP_TYPE) {
      const group = getGroupFromID(taskGroups, activeID);
      if (group === undefined) {
        await log(LogLevel.Error, `Dragged group ${activeID} not found in task group list on drag start, so could not reorder dragged group.`);
        return;
      }

      setDraggedGroup({
        name: group.name,
        numTasks: group.tasks.length,
      });
    }
  };

  const handleDragOver = async ({
    active,
    over,
  }: DragOverEvent) => {
    if (over === null) {
      await log(LogLevel.Error, 'Task to reorder dragged task to is missing on drag over, so could not drag task to new group.');
      return;
    }

    if (over.data.current === undefined) {
      await log(LogLevel.Error, 'Ref for task to reorder dragged task to is undefined on drag over, so could not drag task to new group.');
      return;
    }

    if (over.data.current.type === undefined) {
      await log(LogLevel.Error, 'Task to reorder dragged task to is missing type property on drag over, so could not drag task to new group.');
      return;
    }

    if (active.rect.current.initial === null) {
      await log(LogLevel.Error, 'Dragged task is missing initial rect on drag over, so could not drag task to new group.');
      return;
    }

    const activeID = Number((active.id as string).split('-')[1]);
    const overID = Number((over.id as string).split('-')[1]);
    const overType = over.data.current.type as string;

    if (draggableType === constants.TASK_TYPE
      && overType === constants.TASK_TYPE
      && activeID !== overID) {
      const isDraggingDown = active.rect.current.initial.top < over.rect.top;
      await moveTaskToNewGroup(activeID, overID, isDraggingDown);
    }
  };

  const handleDragEnd = async ({
    active,
    over,
  }: DragEndEvent) => {
    setDraggedTask(null);
    setDraggedGroup(null);

    if (over === null) {
      await log(LogLevel.Error, 'Task/group to reorder dragged task/group to is missing on drag end, so could not reorder task/group.');
      return;
    }

    if (over.data.current === undefined) {
      await log(LogLevel.Error, 'Ref for task/group to reorder dragged task/group to is undefined on drag end, so could not reorder task/group.');
      return;
    }

    const activeID = Number((active.id as string).split('-')[1]);
    const overID = Number((over.id as string).split('-')[1]);
    const overType = over.data.current.type as string;

    if (draggableType === constants.TASK_TYPE
      && overType === constants.TASK_TYPE) {
      await moveTaskToNewOrder(activeID, overID);
    }

    if (draggableType === constants.TASK_GROUP_TYPE
      && overType === constants.TASK_GROUP_TYPE
      && activeID !== overID) {
      await reorderTaskGroup(activeID, overID);
    }
  };

  return (
    <Flex
      direction="column"
      flex={1}
      width="100%"
    >
      <DndContext
        sensors={sensors}
        measuring={{ droppable: { strategy: MeasuringStrategy.Always } }}
        collisionDetection={handleCollisionDetection}
        onDragStart={handleDragStart}
        onDragOver={handleDragOver}
        onDragEnd={handleDragEnd}
      >
        <SortableContext
          items={taskGroups.map((group) => `group-${group.id}`)}
          strategy={verticalListSortingStrategy}
        >
          <Virtuoso
            style={{
              display: 'flex',
              flexDirection: 'column',
              flex: 1,
            }}
            data={taskGroups}
            itemContent={(_, group) => (
              <SortableTaskGroup
                key={group.id}
                id={group.id}
                name={group.name}
                tasks={group.tasks}
              />
            )}
            components={{
              Footer: () => {
                if (isTaskGroupInputOpen) {
                  return (
                    <Flex
                      marginTop={`${addGroupMarginTop}rem`}
                      direction="column"
                      alignItems="center"
                    >
                      <TaskGroupInput
                        mode={TaskGroupInputMode.Adding}
                        isOpen={isTaskGroupInputOpen}
                        width={`${constants.TASK_GROUP.WIDTH}rem`}
                        marginLeft={`${addGroupMarginLeft}rem`}
                        marginBottom={`${scrollMarginBottom}rem`}
                        onClose={() => setIsTaskGroupInputOpen(false)}
                      />
                    </Flex>
                  );
                }
                return (
                  <Flex
                    direction="column"
                    alignItems="center"
                  >
                    <Flex
                      width={`${constants.TASK_GROUP.WIDTH}rem`}
                    >
                      <Button
                        height="3.6rem"
                        fontSize="1.6rem"
                        color={constants.GRAY_400}
                        background="white"
                        marginTop={`${addGroupMarginTop}rem`}
                        marginBottom={`${scrollMarginBottom}rem`}
                        marginLeft={`${addGroupMarginLeft}rem`}
                        justifyContent="flex-start"
                        className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
                        _hover={{
                          color: constants.GRAY_700,
                          background: constants.GRAY_125,
                        }}
                        onClick={() => setIsTaskGroupInputOpen(true)}

                        // Default is 10px.
                        padding={`0rem ${addGroupButtonPadding}rem`}
                      >
                        + Add group
                      </Button>
                    </Flex>
                  </Flex>
                );
              },
            }}
          />
        </SortableContext>
        {createPortal(
          <DragOverlay
            dropAnimation={{
              // Keeps opacity for original dragged item during the drop animation, else the
              // original item disappears during the drop animation.
              sideEffects: defaultDropAnimationSideEffects({
                styles: { active: { opacity: '0' } },
              }),
            }}
          >
            {(() => {
              if (draggedTask !== null && draggedGroup === null) {
                return (
                  <TaskItem
                    task={{
                      id: draggedTask.id,
                      text: draggedTask.text,
                      goal_id: draggedTask.goal_id,
                      color: draggedTask.color,
                      is_completed: draggedTask.is_completed,
                      group_id: draggedTask.group_id,
                    }}
                    isDragOverlay
                  />
                );
              } if (draggedGroup !== null && draggedTask === null) {
                return (
                  <StyledBackdrop
                    invisible
                    open={draggedGroup !== null}
                  >
                    <Flex
                      width={`${constants.DRAG_HANDLE.WIDTH + constants.TASK_ITEM.WIDTH}rem`}
                      padding={`${constants.TASK_GROUP.OVERLAY_PADDING}rem`}
                      background="white"
                      border={`${constants.GRAY_200} solid ${groupDragOverlay.border}rem`}
                      borderRadius="0.4rem"
                      boxShadow={constants.DRAG_OVERLAY_SHADOW}
                      alignItems="center"
                      marginLeft="2.4rem"
                    >
                      <Text
                        color={constants.GRAY_1000}
                        fontSize="1.6rem"
                        lineHeight={`${groupDragOverlay.lineHeight}rem`}
                      >
                        {draggedGroup.name}
                      </Text>
                      <Text
                        color={constants.GRAY_700}
                        fontSize="1.4rem"
                        marginLeft="0.8rem"
                      >
                        {`(${draggedGroup.numTasks} tasks)`}
                      </Text>
                    </Flex>
                  </StyledBackdrop>
                );
              }

              return null;
            })()}
          </DragOverlay>,
          document.body,
        )}
      </DndContext>
    </Flex>
  );
}

export default TaskGroupList;
