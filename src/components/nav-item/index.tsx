import {
  As,
  Button,
  Link,
  Icon,
} from '@chakra-ui/react';
import { Link as RRLink } from 'react-router-dom';

import * as constants from '../../constants';

type Props = {
  path: string;
  text: string;
  icon: As;
  color: string;
  background: string;
  onClick: () => void;
};

function NavItem(props: Props) {
  const {
    path,
    text,
    icon,
    color,
    background,
    onClick,
  } = props;

  const leftIcon = (
    <Icon
      as={icon}
      color={color}
      boxSize="2.0rem"
    />
  );

  return (
    // Wrap button in link to use use routing library Links with buttons.
    // Source: https://stackoverflow.com/questions/42463263/wrapping-a-react-router-link-in-an-html-button
    <Link
      _hover={{ textDecoration: 'none' }}
      as={RRLink}
      to={path}
    >
      <Button
        _hover={{ background: constants.GRAY_200 }}
        background={background}
        color={color}
        fontSize="1.8rem"
        fontWeight="bold"
        leftIcon={leftIcon}
        iconSpacing="1.6rem"
        justifyContent="flex-start"
        width="22.4rem"
        height="3.6rem"
        padding="0 2.4rem 0 2.4rem"
        className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
        onClick={onClick}
      >
        {text}
      </Button>
    </Link>
  );
}

export default NavItem;
