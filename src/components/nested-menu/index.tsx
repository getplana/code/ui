import {
  Grow,
  MenuList,
  Paper,
  Popper,
  PopperProps,
  withStyles,
} from '@material-ui/core';

import * as constants from '../../constants';

const StyledMenuList = withStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
})(MenuList);

interface Props extends PopperProps {
  isNotScrollable?: boolean;
}

function NestedMenu(props: Props) {
  const {
    isNotScrollable,
    children,
    ...rest
  } = props;

  return (
    // NestedMenu essentially recreates Material UI's Menu component using Popper instead of
    // Popover. The reason for doing so is because Popper updates with the available area in the
    // viewport, whereas Popover does not.
    <Popper
      transition
      placement="right"
      style={{
        // Positions nested menu above parent menu, whose default z-index is set to 1300 by Material
        // UI. (Material UI sets the default z-index of Modal to 1300, and Menu inherits from
        // Popover which inherits from Modal.)
        //
        // Default z-index is 1300: https://github.com/mui/material-ui/blob/736f89f1e199dba07b06b969e7bfc3e0b2142a5b/docs/data/material/customization/z-index/z-index.md)
        zIndex: 1301,
      }}
      modifiers={{
        // Prevents popper from being positioned outside the viewport
        preventOverflow: {
          enabled: true,
          boundariesElement: 'viewport',
        },

        // Flips popper's placement when it starts to overlap the viewport.
        flip: {
          enabled: true,
        },
      }}

      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
    >
      {({ TransitionProps }) => (
        <Grow
          timeout={200}

          // eslint-disable-next-line react/jsx-props-no-spreading
          {...TransitionProps}
        >
          <Paper
            elevation={constants.MENU_ELEVATION}
            style={isNotScrollable ? {} : {
              maxHeight: `${constants.MENU.HEIGHT}rem`,
              overflowY: 'auto',
            }}
          >
            <StyledMenuList>
              {children}
            </StyledMenuList>
          </Paper>
        </Grow>
      )}
    </Popper>
  );
}

export default NestedMenu;
