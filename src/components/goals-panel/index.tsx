import {
  Flex,
  VStack,
} from '@chakra-ui/layout';
import {
  Icon,
  IconButton,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Text,
} from '@chakra-ui/react';
import { useState } from 'react';

import { ReactComponent as RightChevronIcon } from '../../assets/chevron-right.svg';
import { ReactComponent as PlusIcon } from '../../assets/plus.svg';
import * as constants from '../../constants';
import { ColorsProvider } from '../../context/colors';
import GoalDialog from '../goal-dialog';
import GoalList from '../goal-list';
import Tab from '../tab';

import './style.scss';

const width = 38.4;
const marginLeftRight = 2.4;
const contentWidth = width - (marginLeftRight * 2);
const headerIconDimension = '2.4rem';
const headerButtonDimension = '3.2rem';
const tabDefaultColor = constants.GRAY_400;

type Props = {
  isOpen: boolean;
  onClose: () => void;
};

function GoalsPanel(props: Props) {
  const {
    isOpen,
    onClose,
  } = props;

  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const [isGoalDialogOpen, setIsGoalDialogOpen] = useState(false);

  return (
    <>
      <ColorsProvider>
        <VStack
          width={isOpen ? `${width}rem` : '0rem'}
          spacing="2.4rem"
          background="none"
          borderLeftColor={constants.GRAY_200}
          borderLeftStyle="solid"
          borderLeftWidth="0.2rem"
          overflow="hidden"
          transition={`width ${constants.PANEL_DURATION}s`}
        >
          <Flex
            width={`${contentWidth}rem`}
            marginTop="3.2rem"
            alignItems="center"
          >
            <IconButton
              icon={(<Icon as={RightChevronIcon} boxSize={headerIconDimension} />)}
              aria-label="Collapse goals panel"
              className={`${constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}`}
              color={constants.GRAY_800}
              width={headerButtonDimension}
              height={headerButtonDimension}
              background="none"
              borderRadius="0.4rem"
              onClick={onClose}
              _hover={{
                color: constants.GRAY_1000,
                background: constants.GRAY_200,
              }}
            />
            <Text
              fontSize="3rem"
              fontWeight="bold"
              color={constants.GRAY_1000}
              marginLeft="2.4rem"
            >
              Goals
            </Text>
            <IconButton
              icon={(<Icon as={PlusIcon} boxSize={headerIconDimension} />)}
              aria-label="Add goal"
              className={`${constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}`}
              color={constants.GRAY_800}
              width={headerButtonDimension}
              height={headerButtonDimension}
              background="none"
              borderRadius="0.4rem"
              marginLeft="auto"
              isDisabled={selectedTabIndex === 1}
              onClick={() => setIsGoalDialogOpen(true)}
              _hover={{
                color: constants.GRAY_1000,
                background: constants.GRAY_200,
              }}
            />
          </Flex>
          <Tabs
            isFitted
            isLazy
            width={`${contentWidth}rem`}
            onChange={(index) => setSelectedTabIndex(index)}

            // Makes goals list scrollable.
            display="flex"
            flexDirection="column"
            minHeight={0}
          >
            <TabList
              borderColor={tabDefaultColor}
            >
              <Tab
                isSelected={selectedTabIndex === 0}
                defaultColor={tabDefaultColor}
              >
                Current
              </Tab>
              <Tab
                isSelected={selectedTabIndex === 1}
                defaultColor={tabDefaultColor}
              >
                Archived
              </Tab>
            </TabList>
            <TabPanels
              className="tab-panels"
              marginTop="1.6rem"
              overflowY="auto"

              // Fitted tabs for Chakra UI take 100% width of container, which in this case has
              // left/right margin. Setting left margin for panels as negative pushes it to start at
              // the left edge of the goals panel.
              marginLeft={`-${marginLeftRight}rem`}
            >
              <TabPanel padding="0rem">
                <GoalList
                  isActive
                  width={width}
                />
              </TabPanel>
              <TabPanel padding="0rem">
                <GoalList
                  isActive={false}
                  width={width}
                />
              </TabPanel>
            </TabPanels>
          </Tabs>
        </VStack>
        <GoalDialog
          isOpen={isGoalDialogOpen}
          onClose={() => setIsGoalDialogOpen(false)}
        />
      </ColorsProvider>
    </>
  );
}

export default GoalsPanel;
