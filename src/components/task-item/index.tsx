import { Flex } from '@chakra-ui/layout';
import {
  Button,
  Icon,
  IconButton,
  Text,
  useToast as useSnackbar,
} from '@chakra-ui/react';
import { DraggableSyntheticListeners } from '@dnd-kit/core';
import {
  ChangeEvent,
  memo,
  MouseEvent,
  useRef,
  useState,
} from 'react';
import { useDebouncedCallback } from 'use-debounce';

import { ReactComponent as RightChevronIcon } from '../../assets/chevron-right.svg';
import { ReactComponent as DragHandleIcon } from '../../assets/drag.svg';
import { ReactComponent as EllipsisIcon } from '../../assets/ellipsis.svg';
import { ReactComponent as GoalIcon } from '../../assets/goal.svg';
import { ReactComponent as TrashIcon } from '../../assets/trash.svg';
import * as constants from '../../constants';
import { useGoals } from '../../context/goals';
import { useTasks } from '../../context/tasks';
import useMenu from '../../hooks/use-menu';
import {
  HSLModel,
  TaskModel,
} from '../../types';
import getHslString from '../../utils/get-hsl-string';
import ActionMenuItem, { Props as ActionMenuItemProps } from '../action-menu-item';
import Checkbox from '../checkbox';
import GoalMenuItem from '../goal-menu-item';
import Menu from '../menu';
import NestedMenu from '../nested-menu';
import Snackbar from '../snackbar';
import Textarea from '../textarea';

export type TaskItemProps = {
  task: TaskModel;
};

interface Props extends TaskItemProps {
  listeners?: DraggableSyntheticListeners;
  isDragging?: boolean;
  isDragOverlay?: boolean;
}

function TaskItem(props: Props) {
  const {
    task,
    isDragging,
    isDragOverlay,
    listeners,
  } = props;

  const [text, setText] = useState(task.text);
  const [isSelected, setIsSelected] = useState(false);
  const [goalMenuAnchor, setGoalMenuAnchor] = useState<HTMLElement | null>(null);
  const [isSnackbarAnimating, setIsSnackbarAnimating] = useState(false);
  const actionMenu = useMenu();
  const snackbar = useSnackbar();
  const undoClicked = useRef(false);
  const { goals } = useGoals(true);
  const {
    updateTask,
    deleteTaskClient,
    undeleteTaskClient,
    deleteTaskServer,
  } = useTasks();

  const debounce = useDebouncedCallback(async (debouncedText: string) => {
    await updateTask(task.id, task.group_id, {
      text: debouncedText,
    });
  }, 500);

  // Use state instead of CSS to still trigger on drop.
  const [isHovering, setIsHovering] = useState(false);

  // --- Handlers: Checkbox --- //

  const handleCheck = async (event: ChangeEvent<HTMLInputElement>) => {
    await updateTask(task.id, task.group_id, {
      isChecked: event.target.checked,
    });
  };

  // --- Handlers: More Button --- //

  const handleMoreButtonClick = (event: MouseEvent<HTMLElement>) => {
    actionMenu.openAtElement(event.currentTarget);
    setIsSelected(true);
  };

  // --- Handlers: Right Click --- //

  const handleRightClick = (event: MouseEvent) => {
    event.preventDefault();

    const position = {
      left: event.pageX,
      top: event.pageY,
    };

    actionMenu.openAtPosition(position);
    setIsSelected(true);
  };

  // --- Handlers: Snackbar --- //

  const handleSnackbarUndo = async () => {
    snackbar.closeAll();
    undoClicked.current = true;
    await undeleteTaskClient();
  };

  const handleSnackbarCloseComplete = async (taskID: number) => {
    if (!undoClicked.current) {
      await deleteTaskServer(taskID);
    }
  };

  // --- Handlers: Action Menu --- //

  const handleChangeGoalHover = (event: MouseEvent<HTMLElement>) => {
    // Source: https://stackoverflow.com/questions/65575013/event-data-inside-settimeout-function-is-null-in-reactjs
    const { currentTarget } = event;

    // Prevents goal menu from opening if snackbar is still animating.
    if (!isSnackbarAnimating) {
      setGoalMenuAnchor(currentTarget);
    }
  };

  const handleDeleteClick = () => {
    setIsSnackbarAnimating(true);

    // Source for duration: https://github.com/chakra-ui/chakra-ui/blob/%40chakra-ui/react%401.6.4/packages/toast/src/toast.tsx#L46
    setTimeout(() => setIsSnackbarAnimating(false), 400);

    // Close action menu.
    actionMenu.close();

    // Remove background from selected task.
    setIsSelected(false);

    // Remove deleted task from client state.
    deleteTaskClient(task.id, task.group_id);

    // Close any previous snackbar.
    snackbar.closeAll();

    // Open snackbar.
    snackbar({
      position: 'bottom',
      duration: 5000,
      render: () => (
        <Snackbar
          onClose={() => snackbar.closeAll()}
        >
          <Text
            fontSize="1.4rem"
            marginLeft="0.8rem"
          >
            Task deleted.
          </Text>
          <Button
            _hover={{ background: constants.GRAY_800 }}
            className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
            fontSize="1.2rem"
            fontWeight="bold"
            background="none"
            marginLeft="auto"
            onClick={handleSnackbarUndo}
          >
            Undo
          </Button>
        </Snackbar>
      ),
      onCloseComplete: () => handleSnackbarCloseComplete(task.id),
    });
  };

  const handleMenuClose = () => {
    actionMenu.close();
    setIsSelected(false);
  };

  // --- Handlers: Goal Menu --- //

  const handleGoalClick = async (goalID: number, goalColor: HSLModel) => {
    setGoalMenuAnchor(null);
    actionMenu.close();
    setIsSelected(false);

    await updateTask(task.id, task.group_id, {
      goalID,
      goalColor,
    });
  };

  // --- Task Actions --- //

  const changeGoalText = 'Change goal';
  const taskActions = [
    {
      text: changeGoalText,
      isSelected: goalMenuAnchor !== null,
      leftIcon: GoalIcon,
      rightIcon: RightChevronIcon,
      onMouseEnter: handleChangeGoalHover,
      onMouseLeave: () => setGoalMenuAnchor(null),
    },
    {
      text: 'Delete',
      leftIcon: TrashIcon,
      onClick: () => handleDeleteClick(),
    },
  ] as ActionMenuItemProps[];

  return (
    <>
      <Flex
        alignItems="flex-start"
        visibility={isDragging ? 'hidden' : undefined}
        onMouseOver={() => setIsHovering(true)}
        onMouseLeave={() => setIsHovering(false)}
      >
        <Flex
          _hover={{ background: isDragOverlay ? undefined : constants.GRAY_150 }}
          padding={`${constants.DRAG_HANDLE.PADDING}rem`}
          borderRadius="0.4rem"
          marginTop="0.8rem"
          cursor={isDragOverlay ? undefined : 'move'}

          // eslint-disable-next-line react/jsx-props-no-spreading
          {...listeners}
        >
          <Icon
            as={DragHandleIcon}
            color={constants.GRAY_800}
            width={`${constants.DRAG_HANDLE.ICON_WIDTH}rem`}
            height="1.4rem"
            visibility={isHovering && !isDragOverlay ? 'visible' : 'hidden'}
          />
        </Flex>
        <Flex
          width={`${constants.TASK_ITEM.WIDTH}rem`}
          paddingTop={`${constants.TASK_ITEM.PADDING}rem`}
          paddingBottom={`${constants.TASK_ITEM.PADDING}rem`}
          paddingLeft={`${constants.TASK_ITEM.PADDING}rem`}
          paddingRight="0rem"
          background={isSelected ? constants.GRAY_125 : 'white'}
          border={isDragOverlay ? `${constants.GRAY_200} solid 0.1rem` : undefined}
          borderRadius="0.4rem"
          boxShadow={isDragOverlay ? constants.DRAG_OVERLAY_SHADOW : undefined}
          alignItems="center"
          onContextMenu={handleRightClick}

          // For absolute position for checkbox.
          position="relative"
        >
          <Flex
            alignSelf="flex-start"
            marginTop="0.4rem"
          >
            <Checkbox
              isChecked={task.is_completed}
              onChange={handleCheck}
            />
          </Flex>
          <Textarea
            value={text}
            color={getHslString(task.color)}
            textDecoration={task.is_completed ? 'line-through' : 'none'}
            marginLeft="0.8rem"
            onChange={async (event: ChangeEvent<HTMLTextAreaElement>) => {
              setText(event.currentTarget.value);
              await debounce(event.currentTarget.value);
            }}
          />
          <IconButton
            className={`${constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}`}
            icon={(<Icon as={EllipsisIcon} boxSize="1.6rem" />)}
            aria-label="Task actions"
            color={constants.GRAY_700}
            minWidth="2.4rem"
            height="2.4rem"
            background="none"
            borderRadius="0.4rem"
            marginLeft="0.8rem"
            visibility={(isHovering && !isDragOverlay) || isSelected ? 'visible' : 'hidden'}
            transition="visibility 0s"
            onClick={handleMoreButtonClick}
            _hover={{
              color: constants.GRAY_900,
              background: constants.GRAY_150,
            }}
          />
        </Flex>
      </Flex>
      <Menu
        open={actionMenu.anchorPosition !== null || actionMenu.anchorElement !== null}
        anchorReference={actionMenu.anchorType}
        anchorEl={actionMenu.anchorElement}
        anchorPosition={actionMenu.anchorPosition ?? undefined}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: actionMenu.anchorType === constants.ANCHOR_TYPE_ELEMENT ? 'center' : 'left',
        }}
        disableBackdrop={goalMenuAnchor !== null}
        onClose={handleMenuClose}
      >
        {taskActions.map((action, index) => (
          <ActionMenuItem
            text={action.text}
            isSelected={action.isSelected}
            leftIcon={action.leftIcon}
            rightIcon={action.rightIcon}
            onMouseEnter={action.onMouseEnter}
            onMouseLeave={action.onMouseLeave}
            onClick={action.onClick}

            // eslint-disable-next-line react/no-array-index-key
            key={index}
          >
            {action.text === changeGoalText && (
              <NestedMenu
                open={goalMenuAnchor !== null}
                anchorEl={goalMenuAnchor}
              >
                {goals.map((goal) => (
                  <GoalMenuItem
                    key={goal.id}
                    goal={goal}
                    isSelected={task.goal_id === goal.id}
                    onClick={() => handleGoalClick(goal.id, goal.color)}
                  />
                ))}
              </NestedMenu>
            )}
          </ActionMenuItem>
        ))}
      </Menu>
    </>
  );
}

export default memo(TaskItem);
