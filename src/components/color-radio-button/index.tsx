import {
  Box,
  Flex,
} from '@chakra-ui/layout';
import {
  Icon,
  useRadio,
} from '@chakra-ui/react';

import { ReactComponent as LetterAMediumIcon } from '../../assets/letter-a-medium.svg';
import * as constants from '../../constants';
import getHslString from '../../utils/get-hsl-string';

// NOTE: Setting type for props as 'any' because TypeScript was for some reason typing any variable
// used for the color prop as 'string | HSLModel' instead of 'HSLModel'.
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function ColorRadioButton(props: any) {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const {
    color,
    ...rest
  } = props;

  const {
    getInputProps,
    getCheckboxProps,
  } = useRadio(rest);
  const inputProps = getInputProps();
  const checkboxProps = getCheckboxProps();

  return (
    <Box as="label">
      <input
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...inputProps}
      />
      <Flex
        _checked={{ border: `solid ${constants.GRAY_800} 0.2rem` }}
        border={`solid ${constants.GRAY_200} 0.2rem`}
        borderRadius="0.4rem"
        padding="0.4rem"
        transition="border 0.2s"
        cursor="pointer"

        // eslint-disable-next-line react/jsx-props-no-spreading
        {...checkboxProps}
      >
        <Icon
          as={LetterAMediumIcon}
          color={getHslString(color)}
          boxSize="1.2rem"
        />
      </Flex>
    </Box>
  );
}

export default ColorRadioButton;
