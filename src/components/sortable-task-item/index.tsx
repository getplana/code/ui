import { Flex } from '@chakra-ui/layout';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

import * as constants from '../../constants';
import TaskItem, { TaskItemProps } from '../task-item';

// Source: https://docs.dndkit.com/api-documentation/draggable/drag-overlay#presentational-components
function SortableTaskItem(props: TaskItemProps) {
  const { task } = props;

  const {
    setNodeRef,
    listeners,
    transform,
    transition,
    isDragging,
  } = useSortable({
    id: `task-${task.id}`,
    data: {
      type: constants.TASK_TYPE,
    },
  });

  return (
    <Flex
      ref={setNodeRef}
      transform={transform ? CSS.Translate.toString(transform) : undefined}
      transition={transition}
    >
      <TaskItem
        listeners={listeners}
        isDragging={isDragging}

        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
      />
    </Flex>
  );
}

export default SortableTaskItem;
