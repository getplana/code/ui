import { Box } from '@chakra-ui/react';
import { Popover } from '@material-ui/core';
import {
  ChangeEvent,
  useEffect,
  useRef,
} from 'react';

import * as constants from '../../constants';
import {
  HSLModel,
  LogLevel,
} from '../../types';
import getHslString from '../../utils/get-hsl-string';
import log from '../../utils/log';
import Textarea from '../textarea';

type Props = {
  open: boolean;
  anchorElement: HTMLElement | null;
  text: string;
  color: HSLModel | null;
  onChange: (newText: string) => void;
  onClose: () => void;
};

function TextareaDialog(props: Props) {
  const {
    open,
    anchorElement,
    text,
    color,
    onChange: handleChange,
    onClose: handleClose,
  } = props;

  let hsl: HSLModel;
  if (color !== null) {
    hsl = {
      hue: color.hue,
      saturation: color.saturation,
      lightness: color.lightness,
    };
  } else {
    hsl = constants.BLACK_HSL;
  }

  const textarea = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    async function focusTextarea() {
      if (open) {
        if (textarea.current === null) {
          await log(LogLevel.Warn, 'Ref for textarea dialog is null, so could not automatically focus on it upon opening.');
          return;
        }

        // Set cursor to end of textarea.
        textarea.current.focus();
      }
    }

    void focusTextarea();
  }, [open]);

  return (
    <Popover
      open={open}
      anchorEl={anchorElement}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}

      // The 'open' property was changing to true before the textarea element was rendering. Keeping
      // the dialog mounted ensures that the dialog can be found, which should not be a significant
      // performance hit since the dialog isn't that big.
      keepMounted
    >
      <Box
        width="38.4rem"
        padding="0.6rem"
        border={`solid ${constants.GRAY_200} 0.1rem`}
        borderRadius="0.4rem"
      >
        <Textarea
          autoFocus
          hasBorder
          ref={textarea}
          value={text}
          color={getHslString(hsl)}
          background={constants.GRAY_100}
          padding="0.4rem 1.2rem"
          borderColor={constants.GRAY_200}
          onChange={async (event: ChangeEvent<HTMLTextAreaElement>) => {
            handleChange(event.currentTarget.value);
          }}
        />
      </Box>
    </Popover>
  );
}

export default TextareaDialog;
