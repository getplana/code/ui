import {
  Center,
  Flex,
} from '@chakra-ui/layout';
import {
  Icon,
  IconButton,
  Text,
} from '@chakra-ui/react';
import {
  memo,
  MouseEvent,
} from 'react';
import { Draggable } from 'react-beautiful-dnd';

import { ReactComponent as DotIcon } from '../../assets/dot.svg';
import { ReactComponent as EllipsisIcon } from '../../assets/ellipsis.svg';
import * as constants from '../../constants';
import { GoalModel } from '../../types';
import getHslString from '../../utils/get-hsl-string';

type Props = {
  goal: GoalModel;
  isNumbered: boolean;
  isSelected: boolean;
  panelWidth: number;
  draggableIndex: number;
  onMoreButtonClick: (event: MouseEvent<HTMLElement>, goal: GoalModel) => void;
  onContextMenu: (event: MouseEvent<HTMLElement>, goal: GoalModel) => void;
};

function GoalItem(props: Props) {
  const {
    goal,
    isNumbered,
    isSelected,
    panelWidth,
    draggableIndex,
    onMoreButtonClick: handleMoreButtonClick,
    onContextMenu: handleContextMenu,
  } = props;

  const margin = 1.6;
  const dragHandleWidth = 0.9;
  const dragHandleSpacing = 0.4;
  const width = panelWidth - (margin * 2) - dragHandleWidth - dragHandleSpacing;

  const color = getHslString(goal.color);

  return (
    <Draggable
      index={draggableIndex}
      draggableId={goal.id.toString()}
    >
      {(provided, snapshot) => (
        <Flex
          className={constants.LIST_ITEM_CLASS}

          // eslint-disable-next-line @typescript-eslint/unbound-method
          ref={provided.innerRef}

          /* eslint-disable react/jsx-props-no-spreading */
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          /* eslint-enable react/jsx-props-no-spreading */
        >
          <Flex
            _hover={{ background: constants.GRAY_125 }}
            width={`${width}rem`}
            padding="0.4rem 0.8rem"
            background={isSelected ? constants.GRAY_125 : 'white'}
            borderRadius="0.4rem"
            marginLeft={`${dragHandleSpacing}rem`}
            onContextMenu={(event) => handleContextMenu(event, goal)}

            // Remove styling for dragged item when it is drop animating, else the item is going to
            // look like its snapping into position when reordering.
            border={snapshot.isDragging && !snapshot.isDropAnimating ? ` solid 0.1rem ${constants.GRAY_200}` : 'none'}
            boxShadow={snapshot.isDragging && !snapshot.isDropAnimating ? constants.DRAG_OVERLAY_SHADOW : 'none'}
          >
            <Flex
              width="1.6rem"
              flexShrink={0}
              justifyContent="center"
            >
              {isNumbered ? (
                <Text
                  color={color}
                >
                  {goal.order_number}
                </Text>
              ) : (
                <Icon
                  as={DotIcon}
                  color={color}
                  boxSize="0.8rem"
                  marginTop="0.8rem"
                />
              )}
            </Flex>
            <Text
              color={color}
              flexGrow={1}
              marginLeft="1.6rem"
            >
              {goal.text}
            </Text>
            <Center marginLeft="0.8rem">
              <IconButton
                icon={(<Icon as={EllipsisIcon} boxSize="1.6rem" />)}
                aria-label="Goal actions"
                color={constants.GRAY_800}
                width="2.4rem"
                height="2.4rem"
                background="none"
                borderRadius="0.4rem"
                visibility={isSelected ? 'visible' : 'hidden'}
                transition="visibility 0s"
                onClick={(event) => handleMoreButtonClick(event, goal)}
                _hover={{
                  color: constants.GRAY_1000,
                  background: constants.GRAY_250,
                }}
                className={
                  `${constants.MORE_BUTTON_CLASS}`
                  + ` ${constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}`
                }
              />
            </Center>
          </Flex>
        </Flex>
      )}
    </Draggable>
  );
}

export default memo(GoalItem);
