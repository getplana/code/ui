import {
  Box,
  ListItemText,
  styled,
  SvgIcon,
} from '@material-ui/core';
import {
  makeStyles,
  withStyles,
} from '@material-ui/core/styles';

import { ReactComponent as CheckmarkIcon } from '../../assets/checkmark.svg';
import { ReactComponent as LetterABoldIcon } from '../../assets/letter-a-bold.svg';
import * as constants from '../../constants';
import { HSLModel } from '../../types';
import getHslString from '../../utils/get-hsl-string';
import MenuItem from '../menu-item';

// --- Styles --- //

const useStyles = makeStyles({
  leftIcon: {
    display: 'flex',
    width: '1.2rem',
    height: '1.2rem',
  },
  rightIcon: {
    width: '1.2rem',
    height: '1.2rem',
    marginLeft: '1.2rem',
  },
});

const StyledBox = styled(Box)({
  border: `solid ${constants.GRAY_200} 0.1rem`,
  borderRadius: '0.4rem',
  padding: '0.4rem',
});

const StyledListItemText = withStyles({
  root: {
    // The bordered "A" icon for some reason does not look centered when the left margin for the
    // color name is 16px.
    marginLeft: '1.4rem',
  },
  primary: {
    fontSize: '1.4rem',
  },
})(ListItemText);

// --- Component --- //

type Props = {
  text: string;
  color: HSLModel;
  isSelected: boolean;
  onClick: () => void;
};

function ColorMenuItem(props: Props) {
  const {
    text,
    color,
    isSelected,
    onClick: handleClick,
  } = props;

  const classes = useStyles();

  return (
    <MenuItem
      onClick={handleClick}
    >
      <StyledBox>
        <SvgIcon
          classes={{
            root: classes.leftIcon,
          }}
          component={LetterABoldIcon}
          htmlColor={getHslString(color)}

          // Defining width and height of viewBox as 18x18 scales the SVG path to have a width with
          // an even number. The path element is for some reason not centered in the SVG element, so
          // defining min-x for viewBox as -1 helps visually center the SVG path.
          viewBox="-1 0 18 18"
        />
      </StyledBox>
      <StyledListItemText
        primary={text}
      />
      <SvgIcon
        classes={{
          root: classes.rightIcon,
        }}
        component={CheckmarkIcon}
        htmlColor={constants.GRAY_800}
        viewBox="0 0 16 12"
        visibility={isSelected ? 'visible' : 'hidden'}
      />
    </MenuItem>
  );
}

export default ColorMenuItem;
