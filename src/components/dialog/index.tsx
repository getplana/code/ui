import {
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Button,
} from '@chakra-ui/react';
import {
  ReactNode,
  RefObject,
} from 'react';

import * as constants from '../../constants';

type ButtonProps = {
  text: string;
  isDisabled?: boolean;
  onClick: () => void;
};

type Props = {
  isOpen: boolean;
  isForm: boolean;
  isDestructive: boolean;
  title: string;
  primary: ButtonProps;
  secondary: ButtonProps;
  children: ReactNode;
  onClose: () => void;

  closeOnOverlayClick?: boolean;
  closeFocusRef?: RefObject<HTMLElement>;
};

function Dialog(props: Props) {
  const {
    isOpen,
    isForm,
    isDestructive,
    closeOnOverlayClick,
    title,
    primary,
    secondary,
    closeFocusRef,
    children,
    onClose: handleClose,
  } = props;

  const fontColor = constants.GRAY_1000;
  const buttonFontSize = '1.2rem';
  const buttonFontWeight = 'bold';
  const buttonHeight = '3.2rem';
  const spacing = '2.4rem';

  return (
    <Modal
      isCentered
      isOpen={isOpen}
      closeOnOverlayClick={closeOnOverlayClick}
      finalFocusRef={closeFocusRef}
      onClose={handleClose}
    >
      <ModalOverlay />
      <ModalContent
        background="white"

        // Override Chakra UI's setting for this.
        maxWidth="44.8rem"
      >
        <ModalHeader
          color={fontColor}
          fontSize="1.8rem"
          fontWeight="bold"
          padding={`2rem ${spacing} 0rem ${spacing}`}
        >
          {title}
        </ModalHeader>
        <ModalBody
          color={fontColor}
          fontSize="1.4rem"
          padding={isForm
            ? `${spacing} ${spacing} 3.2rem ${spacing}`
            : `2rem ${spacing} ${spacing} ${spacing}`}
        >
          {children}
        </ModalBody>
        <ModalFooter
          padding={`0rem ${spacing} ${spacing} ${spacing}`}
        >
          <Button
            className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
            color={constants.GRAY_1000}
            fontSize={buttonFontSize}
            fontWeight={buttonFontWeight}
            background={constants.GRAY_200}
            height={buttonHeight}
            onClick={secondary.onClick}
            _hover={{
              color: constants.GRAY_1100,
              background: constants.GRAY_250,
            }}
          >
            {secondary.text}
          </Button>
          <Button
            _hover={{ background: isDestructive ? constants.RED_800 : constants.GRAY_1200 }}
            className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
            color="white"
            fontSize={buttonFontSize}
            fontWeight={buttonFontWeight}
            background={isDestructive ? constants.RED_600 : constants.GRAY_1000}
            height={buttonHeight}
            minWidth="6.4rem"
            marginLeft="0.8rem"
            isDisabled={primary.isDisabled}
            onClick={primary.onClick}
          >
            {primary.text}
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}

export default Dialog;
