import { HStack } from '@chakra-ui/layout';
import { useRadioGroup } from '@chakra-ui/react';

import { ColorModel } from '../../types';
import ColorRadioButton from '../color-radio-button';

type Props = {
  nameToColor: Map<string, ColorModel>;
  onChange: (name: string) => void;
};

function ColorRadioGroup(props: Props) {
  const {
    nameToColor,
    onChange: handleChange,
  } = props;

  // Value passed to getRadioProps() must be a string or number. It cannot be a custom type
  // (e.g. HSLModel).
  const names = [
    'Red',
    'Orange',
    'Yellow',
    'Green',
    'Teal',
    'Blue',
    'Purple',
    'Magenta',
    'Gray',
  ];

  const {
    getRootProps,
    getRadioProps,
  } = useRadioGroup({
    name: 'color',
    onChange: handleChange,
  });

  const rootProps = getRootProps();

  return (
    <HStack
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rootProps}
    >
      {names.map((name) => {
        const radioProps = getRadioProps({ value: name });
        return (
          <ColorRadioButton
            key={name}
            color={{
              hue: nameToColor.get(name)?.hue,
              saturation: nameToColor.get(name)?.saturation,
              lightness: nameToColor.get(name)?.lightness,
            }}

            // eslint-disable-next-line react/jsx-props-no-spreading
            {...radioProps}
          />
        );
      })}
    </HStack>
  );
}

export default ColorRadioGroup;
