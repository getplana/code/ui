import { Flex } from '@chakra-ui/layout';
import {
  Icon,
  IconButton,
  Input,
} from '@chakra-ui/react';
import {
  ChangeEvent,
  KeyboardEvent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';

import { ReactComponent as CheckmarkThickIcon } from '../../assets/checkmark-thick.svg';
import * as constants from '../../constants';
import { useTasks } from '../../context/tasks';
import {
  LogLevel,
  TaskGroupInputMode,
} from '../../types';
import log from '../../utils/log';

const inputHeight = 4.0;
const inputPaddingMain = 0.6;
const inputPaddingLeft = 1.2;
const inputBorderWidth = 0.1;
const renameButtonDimension = 2.4;

type Props = {
  mode: TaskGroupInputMode;
  isOpen: boolean;
  groupID?: number;
  groupName?: string;
  width?: string;
  marginLeft?: string;
  marginBottom?: string;
  onClose: () => void;
};

// Text input used for adding or renaming task group.
function TaskGroupInput(props: Props) {
  const {
    mode,
    isOpen,
    groupID,
    groupName: initialName,
    width,
    marginLeft,
    marginBottom,
    onClose: handleClose,
  } = props;

  const [groupName, setGroupName] = useState(initialName ?? '');
  const inputRef = useRef<HTMLInputElement>(null);
  const {
    addTaskGroup,
    updateTaskGroup,
  } = useTasks();

  const handleSubmit = useCallback(async () => {
    if (groupName !== '') {
      handleClose();

      if (mode === TaskGroupInputMode.Adding) {
        await addTaskGroup(groupName);
      } else if (mode === TaskGroupInputMode.Renaming && groupID !== undefined) {
        await updateTaskGroup(groupID, { name: groupName });
      }
    }
  }, [handleClose, mode, addTaskGroup, updateTaskGroup, groupID, groupName]);

  useEffect(() => {
    async function focusInput() {
      if (isOpen) {
        if (inputRef.current === null) {
          await log(LogLevel.Warn, 'Ref for task group input is null, so could not automatically focus on it upon opening.');
          return;
        }

        inputRef.current.focus();
      }
    }

    void focusInput();
  }, [isOpen]);

  // NOTE: useOutsideClick() from Chakra UI for some reason would not work when given an async
  // function.
  useEffect(() => {
    const handleOutsideClick = async (event: MouseEvent) => {
      if (inputRef.current
        // Reason for type assertion: https://stackoverflow.com/questions/43842057/detect-if-click-was-inside-react-component-or-not-in-typescript
        && !inputRef.current.contains(event.target as Node)) {
        await handleSubmit();
      }
    };

    /* eslint-disable @typescript-eslint/no-misused-promises */
    document.addEventListener('mousedown', handleOutsideClick);
    return () => document.removeEventListener('mousedown', handleOutsideClick);
    /* eslint-enable @typescript-eslint/no-misused-promises */
  }, [handleSubmit]);

  return (
    <Flex
      width={width ?? undefined}
    >
      <Flex
        width={`${constants.TASK_GROUP.WIDTH
          - constants.DRAG_HANDLE.WIDTH
          - constants.DRAG_HANDLE.MARGIN_RIGHT}rem`}
        padding={`
          ${inputPaddingMain}rem
          ${inputPaddingMain}rem
          ${inputPaddingMain}rem
          ${inputPaddingLeft}rem
        `}
        border={`solid ${constants.GRAY_600} ${inputBorderWidth}rem`}
        borderRadius="0.4rem"
        marginLeft={marginLeft ?? `-${inputPaddingLeft}rem`}
        marginBottom={marginBottom}
        alignItems="center"
      >
        <Input
          ref={inputRef}
          value={groupName}
          color={constants.GRAY_1000}
          fontSize="1.6rem"
          height={`${inputHeight
            - (inputPaddingMain * 2)
            - (inputBorderWidth * 2)}rem`}
          padding="0rem"
          onKeyDown={async (event: KeyboardEvent<HTMLInputElement>) => {
            if (event.key === 'Enter') {
              await handleSubmit();
            }
          }}
          onChange={async (event: ChangeEvent<HTMLInputElement>) => {
            setGroupName(event.currentTarget.value);
          }}

          // Remove focus outline from Chakra UI.
          border="none"
          _focus={{ boxShadow: 'none' }}
        />
        <IconButton
          aria-label="Submit change to task group name"
          icon={(<Icon as={CheckmarkThickIcon} boxSize="1.2rem" />)}
          color={constants.GRAY_800}
          minWidth={`${renameButtonDimension}rem`}
          height={`${renameButtonDimension}rem`}
          marginLeft={`${inputPaddingMain}rem`}
          background="none"
          borderRadius="0.4rem"
          className={`${constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}`}
          _hover={{
            color: constants.GRAY_1000,
            background: constants.GRAY_200,
          }}
          onClick={async () => {
            await handleSubmit();
          }}
        />
      </Flex>
    </Flex>
  );
}

export default TaskGroupInput;
