import {
  Box,
  Grow,
  Menu as MUIMenu,
  MenuProps,
  styled,
} from '@material-ui/core';
import { forwardRef } from 'react';

import * as constants from '../../constants';

// --- Constants --- //

export const nullXY = {
  x: null,
  y: null,
};

// --- Types --- //

export type AttachmentOrigin = {
  vertical: 'bottom' | 'center' | 'top' | number;
  horizontal: 'center' | 'left' | 'right' | number;
};

// --- Component --- //

const StyledBox = styled(Box)({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'stretch',
});

interface Props extends MenuProps {
  isHidden?: boolean;
  disableBackdrop?: boolean;
}

const Menu = forwardRef((props: Props, ref) => {
  const {
    isHidden,
    disableBackdrop,
    children,
    ...rest
  } = props;

  return (
    <MUIMenu
      // NOTE: Attaching ref to PaperProps successfully returned a reference to the menu paper
      // element, but was for some reason causing the menu to be anchored in the top left corner of
      // the screen.
      ref={ref}

      disableRestoreFocus
      elevation={constants.MENU_ELEVATION}
      TransitionComponent={Grow}

      // Must be set to null or undefined when anchorOrigin.vertical is changed from the default.
      // Source: Browser console error from Material UI.
      getContentAnchorEl={null}

      // Using hideBackdrop didn't work.
      style={{
        // Stops backdrop from disabling pointer events.
        pointerEvents: disableBackdrop ? 'none' : 'auto',
      }}
      PaperProps={{
        style: {
          maxHeight: `${constants.MENU.HEIGHT}rem`,
          display: isHidden ? 'none' : 'block',

          // Reenables pointer events for menu itself.
          pointerEvents: 'auto',
        },
      }}

      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
    >
      <StyledBox>
        {children}
      </StyledBox>
    </MUIMenu>
  );
});

export default Menu;
