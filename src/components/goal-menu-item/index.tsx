import {
  ListItemText,
  SvgIcon,
  Typography,
} from '@material-ui/core';
import {
  makeStyles,
  withStyles,
} from '@material-ui/core/styles';

import { ReactComponent as CheckmarkIcon } from '../../assets/checkmark.svg';
import * as constants from '../../constants';
import { GoalModel } from '../../types';
import getHslString from '../../utils/get-hsl-string';
import MenuItem, { style as menuStyle } from '../menu-item';

const useStyles = makeStyles({
  icon: {
    width: '1.2rem',
    height: '1.2rem',
    marginLeft: '0.8rem',
  },
});

type Props = {
  goal: GoalModel;
  isSelected: boolean;
  onClick: () => void;

  width?: string;
  disableHover?: boolean;
  isFocused?: boolean;
  onMouseMove?: () => void;
  onMouseEnter?: () => void;
};

function GoalMenuItem(props: Props) {
  const {
    goal,
    width,
    disableHover,
    isFocused,
    isSelected,
    onClick: handleClick,
    onMouseMove: handleMouseMove,
    onMouseEnter: handleMouseEnter,
  } = props;

  const commonStyle = {
    color: getHslString(goal.color),
    fontFamily: menuStyle.fontFamily,
    fontSize: menuStyle.fontSize,
    lineHeight: menuStyle.lineHeight,
  };

  const StyledNumber = withStyles({
    root: {
      flex: 'none',
      width: '1.6rem',
    },
    primary: {
      ...commonStyle,
      textAlign: 'center',
    },
  })(ListItemText);

  // Typography used instead of ListItemText so that can use Typography's 'noWrap' property.
  const StyledTypography = withStyles({
    root: {
      ...commonStyle,
      flexGrow: 1,
      marginLeft: '1.6rem',
    },
  })(Typography);

  const classes = useStyles();

  return (
    <MenuItem
      width={width ?? '32rem'}
      disableHover={disableHover}
      isFocused={isFocused}
      onMouseMove={handleMouseMove}
      onMouseEnter={handleMouseEnter}
      onClick={handleClick}
    >
      <StyledNumber
        primary={goal.order_number}
      />
      <StyledTypography noWrap>
        {goal.text}
      </StyledTypography>
      <SvgIcon
        classes={{
          root: classes.icon,
        }}
        component={CheckmarkIcon}
        htmlColor={constants.GRAY_800}
        viewBox="0 0 16 12"
        visibility={isSelected ? 'visible' : 'hidden'}
      />
    </MenuItem>
  );
}

export default GoalMenuItem;
