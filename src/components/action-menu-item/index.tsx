import {
  ListItemText,
  SvgIcon,
} from '@material-ui/core';
import {
  makeStyles,
  withStyles,
} from '@material-ui/core/styles';
import {
  ElementType,
  MouseEvent,
  ReactNode,
} from 'react';

import * as constants from '../../constants';
import MenuItem, { style as menuStyle } from '../menu-item';

// --- Styles --- //

const color = constants.GRAY_1000;

const useStyles = makeStyles({
  left: {
    width: '1.6rem',
    height: '1.6rem',
  },
  right: {
    width: '1.2rem',
    height: '1.2rem',
    marginLeft: '1.6rem',
  },
});

const StyledListItemText = withStyles({
  root: {
    marginLeft: '1.6rem',
  },
  primary: {
    color,
    fontFamily: menuStyle.fontFamily,
    fontSize: menuStyle.fontSize,
    lineHeight: menuStyle.lineHeight,
  },
})(ListItemText);

// --- Component --- //

export type Props = {
  text: string;
  leftIcon: ElementType;
  rightIcon?: ElementType;
  isSelected?: boolean;
  onMouseEnter?: (event: MouseEvent<HTMLElement>) => void;
  onMouseLeave?: () => void;
  onClick?: () => void;

  // For nested menu.
  children?: ReactNode;
};

function ActionMenuItem(props: Props) {
  const {
    text,
    isSelected,
    leftIcon,
    rightIcon,
    onMouseEnter: handleMouseEnter,
    onMouseLeave: handleMouseLeave,
    onClick: handleClick,
    children,
  } = props;

  const classes = useStyles();

  return (
    <MenuItem
      isSelected={isSelected}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleClick}
    >
      <SvgIcon
        classes={{
          root: classes.left,
        }}
        component={leftIcon}
        htmlColor={color}
        viewBox="0 0 16 16"
      />
      <StyledListItemText
        primary={text}
      />
      {rightIcon !== undefined && (
        <SvgIcon
          classes={{
            root: classes.right,
          }}
          component={rightIcon}
          htmlColor={constants.GRAY_500}
          viewBox="0 0 16 16"
        />
      )}
      {children}
    </MenuItem>
  );
}

export default ActionMenuItem;
