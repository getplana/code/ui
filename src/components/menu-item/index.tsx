import { MenuItem as MUIMenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  MouseEvent,
  ReactNode,
} from 'react';

import * as constants from '../../constants';

// Source: https://github.com/chakra-ui/chakra-ui/blob/main/packages/components/theme/src/foundations/typography.ts#L43
const chakraUISystemFonts = [
  '-apple-system',
  'BlinkMacSystemFont',
  '"Segoe UI"',
  'Helvetica',
  'Arial',
  'sans-serif',
  '"Apple Color Emoji"',
  '"Segoe UI Emoji"',
  '"Segoe UI Symbol"',
];

export const style = {
  fontSize: '1.4rem',
  lineHeight: '1.6rem',

  // Sets font as system fonts used by Chakra UI instead of Material UI's default, which is Roboto.
  fontFamily: chakraUISystemFonts.join(', '),
};

/**
 * I was getting the following error when spreading props of type MenuItemProps for MenuItem:
 * 'Type 'boolean | undefined' is not assignable to type 'true | undefined'.' It seems the issue was
 * already filed in material-ui’s repo, which had a fix that was already merged. However, it seems
 * the fix was included in v5.0.0 and not v4.x. Until I upgrade to material-ui v5, I'll have to
 * avoid using MenuItemProps for MenuItem.
 *
 * Issue: https://github.com/mui-org/material-ui/issues/16245
 * Fix: https://github.com/mui-org/material-ui/pull/26591
 */
type Props = {
  children: ReactNode;
  isSelected?: boolean;
  onMouseEnter?: (event: MouseEvent<HTMLElement>) => void;
  onMouseMove?: () => void;
  onMouseLeave?: () => void;
  onClick?: () => void;

  // Disabling since ESLint is not registering that the props are being used by makeStyles().
  /* eslint-disable react/no-unused-prop-types */
  width?: string;
  disableHover?: boolean;
  isFocused?: boolean;
  /* eslint-enable react/no-unused-prop-types */
};

const useStyles = makeStyles({
  root: {
    display: 'inline-flex',
    background: (props: Props) => (props.isFocused ? constants.GRAY_150 : 'white'),
    width: (props: Props) => props.width,
    height: `${constants.MENU.ITEM_HEIGHT}rem`,
    minWidth: '12.8rem',
    transition: 'background 0s',
    '&:hover': {
      background: (props: Props) => (props.disableHover && !props.isFocused ? 'none' : constants.GRAY_150),
    },
  },
});

function MenuItem(props: Props) {
  const {
    children,
    isSelected,
    onMouseMove: handleMouseMove,
    onMouseEnter: handleMouseEnter,
    onMouseLeave: handleMouseLeave,
    onClick: handleClick,
  } = props;

  const classes = useStyles(props);

  return (
    <MUIMenuItem
      className={classes.root}
      selected={isSelected}
      onMouseMove={handleMouseMove}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleClick}
    >
      {children}
    </MUIMenuItem>
  );
}

export default MenuItem;
