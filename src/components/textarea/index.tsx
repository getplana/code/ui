import {
  forwardRef,
  Textarea as CUITextarea,
  TextareaProps,
} from '@chakra-ui/react';
import {
  KeyboardEvent,
} from 'react';
import AutosizeTextarea from 'react-textarea-autosize';

interface Props extends TextareaProps {
  hasBorder?: boolean;
  borderColor?: string;
}

const Textarea = forwardRef((props: Props, ref) => {
  const {
    hasBorder,
    borderColor,
    onKeyDown: handleKeyDown,
    ...rest
  } = props;

  return (
    <CUITextarea
      ref={ref}
      as={AutosizeTextarea}
      minH="unset"
      resize="none"
      fontSize="1.4rem"
      padding="0"
      background="none"
      border="unset"
      transition="color 0s"
      onKeyDown={(event: KeyboardEvent<HTMLTextAreaElement>) => {
        if (event.key === 'Enter') {
          event.preventDefault();
        }

        if (handleKeyDown !== undefined) {
          handleKeyDown(event);
        }
      }}

      // Setting border rather than style, color, and width separately was for some reason
      // causing the border to not show.
      borderStyle={hasBorder ? 'solid' : undefined}
      borderColor={hasBorder ? borderColor : undefined}
      borderWidth={hasBorder ? '0.1rem' : undefined}

      // Not setting this was for some reason causing the border to disappear on hover.
      _hover={hasBorder ? {
        borderStyle: 'solid',
        borderColor,
        borderWidth: '0.1rem',
      } : undefined}

      // Hides scrollbars.
      // Source: https://stackoverflow.com/questions/19424887/remove-scrollbars-from-textarea
      overflow="hidden"

      // Remove Chakra UI's focus outline.
      _focus={{ boxShadow: 'none' }}

      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
    />
  );
});

export default Textarea;
