import { Tab as CUITab } from '@chakra-ui/react';
import { ReactNode } from 'react';

import * as constants from '../../constants';

type Props = {
  isSelected: boolean;
  defaultColor: string;
  children: ReactNode;
};

// Must be used in a TabList from Chakra UI, which must be used in a Tabs.
// Source: https://chakra-ui.com/docs/disclosure/tabs.
function Tab(props: Props) {
  const {
    isSelected,
    defaultColor,
    children,
  } = props;

  const selectedColor = constants.GRAY_1000;

  return (
    <CUITab
      className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
      fontSize="1.8rem"
      fontWeight="bold"
      textColor={defaultColor}
      _hover={{ color: isSelected ? selectedColor : constants.GRAY_600 }}
      _active={{ background: 'none' }}
      _selected={{
        color: selectedColor,
        borderColor: selectedColor,
      }}
    >
      {children}
    </CUITab>
  );
}

export default Tab;
