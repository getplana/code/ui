import { Flex } from '@chakra-ui/layout';
import {
  Button,
  Icon,
  IconButton,
  ScaleFade,
  Text,
  useOutsideClick,
} from '@chakra-ui/react';
import { PopoverOrigin } from '@material-ui/core';
import {
  ChangeEvent,
  KeyboardEvent as ReactKeyboardEvent,
  MouseEvent,
  useEffect,
  useRef,
  useState,
} from 'react';

import { ReactComponent as GoalIcon } from '../../assets/goal.svg';
import { ReactComponent as PlusIcon } from '../../assets/plus.svg';
import { ReactComponent as XIcon } from '../../assets/x.svg';
import * as constants from '../../constants';
import { useGoals } from '../../context/goals';
import { useTasks } from '../../context/tasks';
import useMenu from '../../hooks/use-menu';
import {
  GoalModel,
  LogLevel,
} from '../../types';
import getHslString from '../../utils/get-hsl-string';
import log from '../../utils/log';
import Checkbox from '../checkbox';
import GoalMenuItem from '../goal-menu-item';
import Menu from '../menu';
import Textarea from '../textarea';

// --- Constants --- //

const borderWidth = 0.1;
const closeButtonMarginLeft = 0.8;
const iconButton = {
  dimension: 2.4,
  color: constants.GRAY_800,
  hoverColor: constants.GRAY_1000,
  backgroundColor: 'none',
  backgroundHoverColor: constants.GRAY_200,
};

const defaultMenuOrigin = {
  vertical: -4, // Puts space between anchor and menu.
  horizontal: 'left',
} as PopoverOrigin;
const defaultMenuAnchorOrigin = {
  vertical: 'bottom',
  horizontal: 'left',
} as PopoverOrigin;

// --- Component --- //

type Props = {
  isOpen: boolean;
  groupID: number;
  onClose: () => void;
};

function TaskDialog(props: Props) {
  const {
    isOpen,
    groupID,
    onClose: handleClose,
  } = props;

  const [taskText, setTaskText] = useState('');
  const [isChecked, setIsChecked] = useState(false);
  const [selectedGoal, setSelectedGoal] = useState<GoalModel | null>(null);
  const textarea = useRef<HTMLTextAreaElement>(null);
  const taskDialog = useRef<HTMLDivElement>(null);
  const { goals } = useGoals(true);
  const { addTask } = useTasks();

  const goalMenu = useMenu();
  const [menuOrigin, setMenuOrigin] = useState<PopoverOrigin>(defaultMenuOrigin);
  const [menuAnchorOrigin, setMenuAnchorOrigin] = useState<PopoverOrigin>(defaultMenuAnchorOrigin);
  const [isMenuOpenedByKey, setIsMenuOpenedByKey] = useState(false);
  const [menuFilterText, setMenuFilterText] = useState('');
  const [disableMenuItemHover, setDisableMenuItemHover] = useState(false);
  const [focusedMenuItem, setFocusedMenuItem] = useState<number | null>(null);
  const [isMenuHidden, setIsMenuHidden] = useState(false);
  const menuElement = useRef<HTMLElement>(null);

  useEffect(() => {
    if (goals.filter(
      // NOTE: Using filterGoals() in here required it to be memoized with useCallback(). However,
      // memoizing filterGoal() was for some reason causing filtered goals to always be an empty
      // list.
      (goal: GoalModel) => goal.order_number.toString().startsWith(menuFilterText),
    ).length === 0) {
      // Hide goal menu if there are no filtered goals to show.
      setIsMenuHidden(true);
    } else {
      setIsMenuHidden(false);
    }
  }, [goals, menuFilterText]);

  // --- Helpers --- //

  // Filters for goals whose order number starts with the numbers entered after the '@'.
  const filterGoals = (goal: GoalModel) => goal.order_number.toString().startsWith(menuFilterText);

  const closeGoalMenu = async () => {
    goalMenu.close();
    setFocusedMenuItem(0);

    // Filter text was being reset too quickly, probably because goal menu has closing animation.
    setTimeout(() => setMenuFilterText(''), 0);

    if (textarea.current === null) {
      await log(LogLevel.Warn, 'Ref for task dialog textarea is null, so could not automatically focus on it after closing goal menu.');
      return;
    }

    // NOTE: Textarea was not focusing unless used with setTimeout().
    setTimeout(() => textarea.current?.focus(), 0);
  };

  // --- Handlers: Pressing Enter --- //

  const close = () => {
    handleClose();
    setTaskText('');
    setSelectedGoal(null);
  };

  const closeAndAdd = async () => {
    if (taskText !== '' && selectedGoal !== null) {
      handleClose();

      await addTask(
        taskText,
        selectedGoal.id,
        selectedGoal.color,
        isChecked,
        groupID,
      );

      setTaskText('');
      setSelectedGoal(null);
    }
  };

  useEffect(() => {
    const handleKeyDown = async (event: KeyboardEvent) => {
      if (event.key === 'Enter'
          && taskText !== ''
          && selectedGoal !== null
          && goalMenu.anchorElement === null) {
        await closeAndAdd();
      }
    };

    /* eslint-disable @typescript-eslint/no-misused-promises */
    document.addEventListener('keydown', handleKeyDown);
    return () => document.removeEventListener('keydown', handleKeyDown);
    /* eslint-enable @typescript-eslint/no-misused-promises */
  });

  // --- Handlers: Outside Click --- //

  const handleOutsideClick = () => {
    if (goalMenu.anchorElement === null
      && taskText === ''
      && selectedGoal === null) {
      close();
    }
  };

  useOutsideClick({
    ref: taskDialog,
    handler: handleOutsideClick,
  });

  // --- Handlers: Checkbox --- //

  const handleCheckboxChange = (event: ChangeEvent<HTMLInputElement>) => {
    setIsChecked(event.target.checked);
  };

  // --- Handlers: Goal Menu --- //

  const handleGoalMenuSelect = async (goal: GoalModel, cursorPosition: number) => {
    setSelectedGoal(goal);

    if (isMenuOpenedByKey) {
      // Remove space, '@', and filter text from textarea.
      const textBeforeGoalTag = taskText.substring(0, cursorPosition - menuFilterText.length - 2);
      const textafterGoalTag = taskText.substring(cursorPosition);
      setTaskText(textBeforeGoalTag + textafterGoalTag);
    }

    await closeGoalMenu();
  };

  // --- Handlers: Goal Menu Item --- //

  const handleGoalMenuItemClick = async (goal: GoalModel) => {
    if (textarea.current === null) {
      await log(LogLevel.Error, 'Ref for task dialog textarea is null, so could not handle click on goal menu item (since doing so may involve removing \'@\' and filter text from textarea).');
      return;
    }

    const cursorPosition = textarea.current.selectionStart;
    await handleGoalMenuSelect(goal, cursorPosition);
  };

  // --- Handlers: Textarea --- //

  // Anchors goal menu horizontally before given cursor position.
  const openGoalMenuAtTextarea = async () => {
    if (textarea.current === null) {
      await log(LogLevel.Error, 'Ref for task dialog textarea is null, so could not open goal menu at task dialog textarea.');
      return;
    }

    // When the goal menu is opened, the mouse cursor disappears and the first menu item should be
    // highlighted. However, if the cursor was over a menu item that is not the first before it
    // disappeared, the 'hover' CSS pseudo-class for some reason matches to it, causing both it and
    // the first menu item to be highlighted. To prevent this from happening, we want to disable
    // hover styling for menu items when the goal menu first opens, and reenable it only if the user
    // moves the cursor again.
    setDisableMenuItemHover(true);

    setMenuOrigin({
      vertical: 'top',
      horizontal: 'left',
    });
    setMenuAnchorOrigin({
      vertical: 'bottom',
      horizontal: 0,
    });
    setFocusedMenuItem(0);
    setIsMenuOpenedByKey(true);
    goalMenu.openAtElement(textarea.current);
  };

  const handleTextareaClick = async (event: MouseEvent<HTMLTextAreaElement>) => {
    if (textarea.current === null) {
      await log(LogLevel.Warn, 'Ref for task dialog textarea is null, so could not open goal menu on clicking after \'@\'.');
      return;
    }

    const text = textarea.current.value;
    const cursorPosition = event.currentTarget.selectionStart;

    // If the character before the cursor position is '@', then open goal menu. However, only do so
    // if there is a space after '@' or the '@' is at the end.
    if (text[cursorPosition - 1] === '@'
        && (text[cursorPosition] === ' ' || text[cursorPosition] === undefined)) {
      await openGoalMenuAtTextarea();
    }
  };

  const handleTextareaKeyDown = async (event: ReactKeyboardEvent<HTMLTextAreaElement>) => {
    const text = event.currentTarget.value;
    const cursorPosition = event.currentTarget.selectionStart;

    // - If goal menu is not open. - //

    if (goalMenu.anchorElement === null) {
      // If '@' is entered, then open goal menu. However, only do so if there is a space after the
      // entered '@', or the '@' is at the end.
      if (event.key === '@'
          && (text[cursorPosition] === ' ' || text[cursorPosition] === undefined)) {
        await openGoalMenuAtTextarea();
      }

      // If deleting characters brings the cursor back to '@', then open goal menu. However, only
      // do so if there is still a space after '@', or the '@' is at the end.
      if (event.key === 'Backspace'
          && text[cursorPosition - 2] === '@'
          && (text[cursorPosition] === ' ' || text[cursorPosition] === undefined)) {
        await openGoalMenuAtTextarea();
      }

      // NOTE: Handlers for arrow keys would be cleaner to implement in onKeyUp after the cursor
      // position has been updated, but it seems its better to have all of the logic in either
      // onKeyDown or onKeyUp to avoid bugs from onKeyUp being called right after onKeyDown.

      // If cursor is moved to after '@' with left arrow key.
      if (event.key === 'ArrowLeft') {
        // If the character before the cursor is '@', then open goal menu. However, only do so if
        // there is a space after the '@'.
        if (text[cursorPosition - 2] === '@'
            && text[cursorPosition - 1] === ' ') {
          await openGoalMenuAtTextarea();
        }
      }

      // If cursor is moved to after '@' with right arrow key.
      if (event.key === 'ArrowRight') {
        // If the character before the cursor is '@', then open goal menu. However, only do so if
        // there is a space after the '@', or the '@' is at the end.
        if (text[cursorPosition] === '@'
            && (text[cursorPosition + 1] === ' ' || text[cursorPosition + 1] === undefined)) {
          await openGoalMenuAtTextarea();
        }
      }

      // If cursor is moved to after '@' with down arrow key (which moves the cursor to the end).
      if (event.key === 'ArrowDown') {
        const cursorAtEnd = text.length;

        // If the character before the cursor is '@', then open goal menu. However, only do so if
        // the '@' is at the end.
        if (text[cursorAtEnd - 1] === '@'
            && text[cursorAtEnd] === undefined) {
          await openGoalMenuAtTextarea();
        }
      }
    }

    // - If goal menu is opened. - //

    if (goalMenu.anchorElement !== null) {
      if (event.key === 'ArrowDown'
        || event.key === 'ArrowRight'
        || event.key === 'ArrowUp'
        || event.key === 'ArrowLeft') {
        if (menuElement.current === null) {
          await log(LogLevel.Error, 'Ref for textarea goal menu in task dialog is null, so could not navigate through goal menu with arrow keys.');
          return;
        }

        // Prevent cursor on textarea from moving.
        event.preventDefault();

        // Navigating through the goal menu with arrow keys involves highlighting the next menu item
        // when the down or right arrow key is pressed, and highlighting the previous one when the
        // up or left arrow key is pressed. When the user navigates through the goal menu with arrow
        // keys, we want to disable highlighting menu items with the mouse cursor so that we don't
        // have two menu items being highlighted. We can then reenable it only if the user moves the
        // cursor again after using the arrow keys on the goal menu.
        setDisableMenuItemHover(true);

        const filteredGoalsLength = goals.filter(filterGoals).length;

        // NOTE: Assumes that the Paper element is the 3rd child of the Popover element.
        const menuPaperElement = menuElement.current.children[2];

        if (event.key === 'ArrowDown' || event.key === 'ArrowRight') {
          if (focusedMenuItem === null || focusedMenuItem === filteredGoalsLength - 1) {
            // If user presses down/right arrow key after (a) using mouse on goal menu or (b)
            // navigating to last menu item with arrow keys, then set focus to first menu item.
            setFocusedMenuItem(0);
            menuPaperElement.scrollTop = 0;
          } else {
            // Else, focus on the next menu item.
            setFocusedMenuItem(focusedMenuItem + 1);
          }

          // Scroll menu down if focus is on 6th item from the top, and user wants to focus on the
          // next item.
          if (focusedMenuItem !== null
            && focusedMenuItem >= 5
            && focusedMenuItem < filteredGoalsLength - 1) {
            menuPaperElement.scrollTop += (constants.MENU.ITEM_HEIGHT * 10);
          }

          return;
        }

        if (event.key === 'ArrowUp' || event.key === 'ArrowLeft') {
          if (focusedMenuItem === null || focusedMenuItem === 0) {
            // If user presses up/left arrow key after (a) using mouse on goal menu or (b)
            // navigating to first menu item with arrow keys, then set focus to last menu item.
            setFocusedMenuItem(filteredGoalsLength - 1);
            menuPaperElement.scrollTop = menuPaperElement.scrollHeight;
          } else {
            // Else, focus on the previous menu item.
            setFocusedMenuItem(focusedMenuItem - 1);
          }

          // Scroll menu up if focus is on 6th item from the bottom, and user wants to focus on the
          // previous item.
          if (focusedMenuItem !== null
            && focusedMenuItem <= (filteredGoalsLength - 1) - 5
            && focusedMenuItem > 0) {
            menuPaperElement.scrollTop -= (constants.MENU.ITEM_HEIGHT * 10);
          }

          return;
        }
      }

      if (event.key === 'Enter') {
        // If the user hovers over a menu item, but presses enter.
        if (focusedMenuItem === null) {
          return;
        }

        // Prevent pressing 'Enter' from adding task for task dialog.
        event.stopPropagation();

        // Select focused goal.
        await handleGoalMenuSelect(goals.filter(filterGoals)[focusedMenuItem], cursorPosition);
        return;
      }

      if (event.key === 'Escape') {
        await closeGoalMenu();
        return;
      }

      if (event.key === 'Backspace') {
        const charToDelete = text[cursorPosition - 1];
        if (charToDelete === '@') {
          // If '@' is deleted, then close goal menu.
          await closeGoalMenu();
        } else {
          // Else, update filter text for goal menu.
          setMenuFilterText((prev) => prev.substring(0, prev.length - 1));
          setFocusedMenuItem(0);
        }

        return;
      }

      if (event.key === ' ') {
        await closeGoalMenu();
        return;
      }

      // If anything else is entered (except for the following keys), then filter goal menu.
      if (event.key !== 'Alt'
        && event.key !== 'CapsLock'
        && event.key !== 'Control'
        && event.key !== 'Meta'
        && event.key !== 'Shift'
        && event.key !== 'Tab') {
        setMenuFilterText((prev) => prev + event.key);
        setFocusedMenuItem(0);
      }
    }
  };

  return (
    <>
      <ScaleFade
        in={isOpen}
        unmountOnExit
        transition={{
          enter: { duration: 0 },
          exit: { duration: 0 },
        }}
      >
        <Flex
          marginTop="0.8rem"
          marginLeft={`${constants.DRAG_HANDLE.WIDTH
            + constants.DRAG_HANDLE.MARGIN_RIGHT
            + constants.DRAG_HANDLE.WIDTH}rem`}
          alignItems="center"
        >
          <Flex
            ref={taskDialog}
            width={`${constants.TASK_ITEM.WIDTH
                - iconButton.dimension
                - closeButtonMarginLeft}rem`}
            padding={`${constants.TASK_ITEM.PADDING}rem`}
            borderStyle="solid"
            borderColor={constants.GRAY_600}
            borderWidth={`${borderWidth}rem`}
            borderRadius="0.4rem"
            flex={1}
            alignItems="center"
            boxShadow={constants.TASK_DIALOG_SHADOW}
          >
            <Flex
              alignSelf="flex-start"
              marginTop="0.2rem"
            >
              <Checkbox
                isChecked={isChecked}
                onChange={handleCheckboxChange}
              />
            </Flex>
            <Flex
              flexDirection="column"
              flex={1}
              alignItems="flex-start"
              marginLeft="0.8rem"
              marginRight="0.4rem"
            >
              <Textarea
                autoFocus
                ref={textarea}
                value={taskText}
                color={selectedGoal
                  ? getHslString(selectedGoal.color)
                  : getHslString(constants.LIGHT_GRAY_HSL)}
                textDecoration={isChecked ? 'line-through' : 'none'}
                onClick={handleTextareaClick}
                onKeyDown={handleTextareaKeyDown}
                onChange={(event: ChangeEvent<HTMLTextAreaElement>) => {
                  setTaskText(event.currentTarget.value);
                }}
              />
              <Button
                className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
                color={selectedGoal ? getHslString(selectedGoal.color) : constants.GRAY_800}
                fontSize="1.4rem"
                fontWeight="normal"
                height="2.8rem"
                background="none"
                padding="0rem 0.8rem"
                marginTop="1.2rem"
                border={`solid 1px ${constants.GRAY_400}`}
                _hover={{
                  color: constants.GRAY_1000,
                  background: constants.GRAY_200,
                }}
                onClick={(event: MouseEvent<HTMLElement>) => {
                  setIsMenuOpenedByKey(false);
                  setMenuOrigin(defaultMenuOrigin);
                  setMenuAnchorOrigin(defaultMenuAnchorOrigin);
                  goalMenu.openAtElement(event.currentTarget);
                }}
              >
                <Icon
                  as={GoalIcon}
                  boxSize="1.6rem"
                />
                <Text
                  isTruncated
                  maxWidth="50rem"
                  marginLeft="0.6rem"
                >
                  {selectedGoal?.text ?? 'Goal'}
                </Text>
              </Button>
            </Flex>
            <IconButton
              icon={(<Icon as={PlusIcon} boxSize="1.4rem" />)}
              aria-label="Add task"
              className={`${constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}`}
              color={`${constants.GRAY_800}`}
              minWidth={`${iconButton.dimension}rem`}
              height={`${iconButton.dimension}rem`}
              background="none"
              borderRadius="0.4rem"
              isDisabled={taskText === '' || selectedGoal === null}
              onClick={async () => closeAndAdd()}
              _hover={taskText === '' || selectedGoal === null ? {} : {
                color: constants.GRAY_1000,
                background: constants.GRAY_200,
              }}
            />
          </Flex>
          <IconButton
            icon={(<Icon as={XIcon} boxSize="1.2rem" />)}
            aria-label="Close task dialog"
            className={`${constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}`}
            color={constants.GRAY_800}
            minWidth={`${iconButton.dimension}rem`}
            height={`${iconButton.dimension}rem`}
            background="none"
            borderRadius="0.4rem"
            marginLeft={`${closeButtonMarginLeft}rem`}
            onClick={() => close()}
            _hover={{
              color: constants.GRAY_1000,
              background: constants.GRAY_200,
            }}
          />
        </Flex>
      </ScaleFade>
      <Menu
        ref={menuElement}
        open={goalMenu.anchorElement !== null}
        isHidden={isMenuHidden}
        anchorReference={constants.ANCHOR_TYPE_ELEMENT}
        anchorEl={goalMenu.anchorElement}
        anchorOrigin={menuAnchorOrigin}
        transformOrigin={menuOrigin}
        disableRestoreFocus={false}
        onClose={() => closeGoalMenu()}

        /**
         * All of the following props were needed to disable focus on goal menu when opening via
         * entering '@' (to keep focus on textarea).
         */

        // Places focus on the parent modal container of the menu.
        // Source: https://v4.mui.com/api/menu/#props
        autoFocus={!isMenuOpenedByKey}

        // Prevents focus from automatically shifting to modal itself when it opens.
        // Source: https://v4.mui.com/api/modal/#props
        disableAutoFocus={isMenuOpenedByKey}

        // Allows focus to leave modal when open.
        // Source: https://v4.mui.com/api/modal/#props
        disableEnforceFocus={isMenuOpenedByKey}
      >
        {goals.filter(filterGoals).map((goal, index) => (
          <GoalMenuItem
            key={goal.id}
            goal={goal}
            width={isMenuOpenedByKey && textarea.current?.clientWidth !== undefined
              ? `${textarea.current.clientWidth}px`
              : undefined}
            disableHover={disableMenuItemHover}
            isFocused={focusedMenuItem === index}
            isSelected={selectedGoal?.id === goal.id}
            onMouseMove={() => {
              setFocusedMenuItem(null);
              setDisableMenuItemHover(false);
            }}
            onClick={() => handleGoalMenuItemClick(goal)}
          />
        ))}
      </Menu>
    </>
  );
}

export default TaskDialog;
