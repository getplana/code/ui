import { VStack } from '@chakra-ui/react';
import { useState } from 'react';

import { ReactComponent as TimelineIcon } from '../../assets/timeline.svg';
import { ReactComponent as CheckedBoxIcon } from '../../assets/today.svg';
import { ReactComponent as TomorrowIcon } from '../../assets/tomorrow.svg';
import * as constants from '../../constants';
import NavItem from '../nav-item';

const today = 'Today';
const tomorrow = 'Tomorrow';
const timeline = 'Timeline';
const defaultColor = constants.GRAY_600;
const selectedColor = constants.GRAY_1000;
const defaultBackgroundColor = constants.GRAY_100;
const selectedBackgroundColor = constants.GRAY_200;

function NavList() {
  const [currPage, setCurrPage] = useState(today);

  return (
    <VStack spacing="0.8rem">
      <NavItem
        path="/"
        text={today}
        icon={CheckedBoxIcon}
        color={currPage === today ? selectedColor : defaultColor}
        background={currPage === today ? selectedBackgroundColor : defaultBackgroundColor}
        onClick={() => setCurrPage(today)}
      />
      <NavItem
        path="/tomorrow"
        text={tomorrow}
        icon={TomorrowIcon}
        color={currPage === tomorrow ? selectedColor : defaultColor}
        background={currPage === tomorrow ? selectedBackgroundColor : defaultBackgroundColor}
        onClick={() => setCurrPage(tomorrow)}
      />
      <NavItem
        path="/timeline"
        text={timeline}
        icon={TimelineIcon}
        color={currPage === timeline ? selectedColor : defaultColor}
        background={currPage === timeline ? selectedBackgroundColor : defaultBackgroundColor}
        onClick={() => setCurrPage(timeline)}
      />
    </VStack>
  );
}

export default NavList;
