import {
  Button,
  Icon,
} from '@chakra-ui/react';

import { ReactComponent as GoalIcon } from '../../assets/goal.svg';
import * as constants from '../../constants';

type Props = {
  isVisible: boolean;
  top: string;
  onClick: () => void;
};

function GoalsPanelButton(props: Props) {
  const {
    isVisible,
    top,
    onClick,
  } = props;

  return (
    <Button
      leftIcon={(<Icon as={GoalIcon} boxSize="1.6rem" />)}
      className={constants.DISABLE_MOUSE_FOCUS_OUTLINE_CLASS}
      fontSize="1.6rem"
      color={constants.GRAY_1000}
      iconSpacing="0.8rem"
      width="10rem"
      height="4rem"
      background={constants.GRAY_200}
      borderRadius="0.8rem 0rem 0rem 0.8rem"
      opacity={isVisible ? '100%' : '0%'}
      transition={`opacity ${constants.PANEL_DURATION}s`}
      position="absolute"
      top={top}
      onClick={onClick}
      _hover={{
        color: constants.GRAY_1100,
        background: constants.GRAY_250,
      }}

      // NOTE: Not sure why this positions the button all the way to the right.
      right="0rem"
    >
      Goals
    </Button>
  );
}

export default GoalsPanelButton;
