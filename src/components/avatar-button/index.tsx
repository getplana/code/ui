import {
  Button,
  Icon,
} from '@chakra-ui/react';

import { ReactComponent as ChevronDownIcon } from '../../assets/chevron-down.svg';
import { ReactComponent as InitialS } from '../../assets/letter-s.svg';
import * as constants from '../../constants';

type Props = {
  customerName: string;
};

const avatar = (
  <Icon
    as={InitialS}
    color={constants.GRAY_600}
    boxSize="2rem"
  />
);

const downChevron = (
  <Icon
    as={ChevronDownIcon}
    color={constants.GRAY_600}
    boxSize="0.8rem"
  />
);

function AvatarButton(props: Props) {
  const { customerName } = props;

  return (
    <Button
      color={constants.GRAY_800}
      fontSize="1.6rem"
      iconSpacing="0.8rem"
      leftIcon={avatar}
      rightIcon={downChevron}
    >
      {customerName}
    </Button>
  );
}

export default AvatarButton;
