export enum TaskGroupInputMode {
  Renaming,
  Adding,
}

export enum DataSource {
  Client,
  Server,
}

export enum LogLevel {
  Error = 'error',
  Warn = 'warn',
}
