import { HSLModel } from './models';

export interface GoalUpdate {
  colorID?: number;
  colorHSL?: HSLModel;
  isActive?: boolean;
}

export interface TaskUpdate {
  text?: string;
  isChecked?: boolean
  goalID?: number;
  goalColor?: HSLModel;
}

export interface TaskGroupUpdate {
  name?: string;
}
