export interface Orderable {
  order_number?: number;
  group_order?: number;
  day_order?: number;
}

export interface DayModel {
  date: string;
  task_groups: TaskGroupModel[];
}

export interface TaskGroupModel {
  id: number;
  name: string;
  day_order: number;
  tasks: TaskModel[];
}

export interface TaskModel extends Orderable {
  id: number;
  text: string;
  goal_id: number;
  goal_text?: string;
  color: HSLModel;
  is_completed: boolean;
  group_id: number;
}

export interface TaskGroupUpdateModel {
  name?: string;
  day_order?: number;
}

export interface TaskUpdateModel {
  text?: string;
  goal_id?: number;
  is_completed?: boolean;
  group_id?: number;
  group_order?: number;
}

export interface GoalModel extends Orderable {
  id: number;
  text: string;
  color: HSLModel;
  order_number: number;
}

export interface HSLModel {
  hue: number;
  saturation: number;
  lightness: number;
}

export interface ColorModel extends HSLModel {
  id: number;
  name: string;
}
