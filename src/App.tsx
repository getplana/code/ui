import { Flex } from '@chakra-ui/layout';
import { Outlet } from 'react-router-dom';

import NavBar from './components/nav-bar';
import getApiUrl from './utils/get-api-url';

function App() {
  // Check and retrieve environment variables.
  getApiUrl();

  return (
    <Flex
      height="100vh"
      background="white"
    >
      <NavBar />
      <Outlet />
    </Flex>
  );
}

export default App;
