import { TaskGroupModel } from '../../types';

export default function isTaskOrderCorrect(groups: TaskGroupModel[]) {
  for (let i = 0; i < groups.length; i += 1) {
    const { tasks } = groups[i];
    let groupOrder = 1;
    for (let j = 0; j < tasks.length; j += 1) {
      if (tasks[j].group_order !== groupOrder) {
        return false;
      }

      groupOrder += 1;
    }
  }

  return true;
}
