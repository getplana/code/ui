import log from 'loglevel';

import { TaskGroupModel } from '../../types';

export default function isTaskOrderEqual(groups1: TaskGroupModel[], groups2: TaskGroupModel[]) {
  if (groups1.length !== groups2.length) {
    log.error('Client and server have different number of groups.');
    return false;
  }

  for (let i = 0; i < groups1.length; i += 1) {
    const group1 = groups1[i];
    const group2 = groups2[i];
    if (group1.id !== group2.id) {
      log.error(`Group #${i + 1} in client is group ${group1.id}, but in server is group ${group2.id}.`);
      return false;
    }

    const { tasks: tasks1 } = group1;
    const { tasks: tasks2 } = group2;
    if (tasks1.length !== tasks2.length) {
      log.error(`Group #${i + 1} in client and server have different number of tasks.`);
      return false;
    }

    for (let j = 0; j < tasks1.length; j += 1) {
      const task1 = tasks1[j];
      const task2 = tasks2[j];
      if (task1.id !== task2.id) {
        log.error(`Task #${j + 1} in group #${i + 1} in client is task ${task1.id}, but in server is task ${task2.id}.`);
        return false;
      }
    }
  }

  return true;
}
