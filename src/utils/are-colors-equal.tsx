import { HSLModel } from '../types';

export default function areColorsEqual(color1: HSLModel, color2: HSLModel) {
  return color1.hue === color2.hue
         && color1.saturation === color2.saturation
         && color1.lightness === color2.lightness;
}
