export default function parseDateString(dateStr: string) {
  const yearStr = dateStr.substring(0, 4);
  const monthStr = dateStr.substring(5, 7);
  const dayStr = dateStr.substring(8, 10);

  const year = Number(yearStr);
  const month = Number(monthStr);
  const day = Number(dayStr);

  return new Date(year, month - 1, day);
}
