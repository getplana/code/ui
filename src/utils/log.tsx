import axios from 'axios';
// eslint-disable-next-line import/no-named-default
import { default as loglevel } from 'loglevel';

import { LogLevel } from '../types';

import getApiUrl from './get-api-url';

export default async function log(level: LogLevel, message: string) {
  if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test') {
    if (level === LogLevel.Error) {
      loglevel.error(message);
    } else if (level === LogLevel.Warn) {
      loglevel.warn(message);
    }
  } else if (process.env.NODE_ENV === 'production') {
    const logData = {
      timestamp: Date.now(),
      level,
      message,
    };

    try {
      await axios.post(`${getApiUrl()}/logs`, logData);
    } catch (error) {
      loglevel.error(`Failed to log error: ${JSON.stringify(logData)}`);
    }
  }
}
