import {
  TaskGroupModel,
  TaskModel,
} from '../../types';

function getTaskFromID(groups: TaskGroupModel[], taskID: number) {
  let foundTask: TaskModel | undefined;
  groups.forEach((group) => {
    group.tasks.forEach((task) => {
      if (task.id === taskID) {
        foundTask = task;
      }
    });
  });

  return foundTask;
}

export default getTaskFromID;
