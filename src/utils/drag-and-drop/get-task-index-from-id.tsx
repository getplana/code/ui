import { TaskModel } from '../../types';

function getTaskIndexFromID(tasks: TaskModel[], taskID: number) {
  let taskIndex: number | undefined;
  tasks.forEach((task, index) => {
    if (task.id === taskID) {
      taskIndex = index;
    }
  });

  return taskIndex;
}

export default getTaskIndexFromID;
