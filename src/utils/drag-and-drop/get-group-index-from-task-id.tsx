import { TaskGroupModel } from '../../types';

function getGroupIndexFromTaskID(groups: TaskGroupModel[], taskID: number) {
  let groupIndex: number | undefined;
  groups.forEach((group, index) => {
    group.tasks.forEach((task) => {
      if (task.id === taskID) {
        groupIndex = index;
      }
    });
  });

  return groupIndex;
}

export default getGroupIndexFromTaskID;
