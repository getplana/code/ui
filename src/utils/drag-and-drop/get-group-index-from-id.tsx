import { TaskGroupModel } from '../../types';

function getGroupIndexFromID(groups: TaskGroupModel[], groupID: number) {
  let groupIndex: number | undefined;
  groups.forEach((group, index) => {
    if (group.id === groupID) {
      groupIndex = index;
    }
  });

  return groupIndex;
}

export default getGroupIndexFromID;
