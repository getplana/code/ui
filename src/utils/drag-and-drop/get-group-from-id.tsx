import { TaskGroupModel } from '../../types';

function getGroupFromID(groups: TaskGroupModel[], groupID: number) {
  let foundGroup: TaskGroupModel | undefined;
  groups.forEach((group) => {
    if (group.id === groupID) {
      foundGroup = group;
    }
  });

  return foundGroup;
}

export default getGroupFromID;
