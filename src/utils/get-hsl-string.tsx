import { HSLModel } from '../types/models';

export default function getHslString(hsl: HSLModel) {
  return `hsl(${hsl.hue}, ${hsl.saturation}%, ${hsl.lightness}%)`;
}
