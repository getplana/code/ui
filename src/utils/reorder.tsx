import cloneDeep from 'lodash.clonedeep';

import { Orderable } from '../types';

function cloneFromUpTo(
  oldList: Orderable[],
  newList: Orderable[],
  from: number,
  upTo: number,
  shiftChange?: number,
) {
  for (let i = from; i < upTo; i += 1) {
    const clone = cloneDeep(oldList[i]);
    if (shiftChange !== undefined) {
      if (clone.order_number !== undefined) clone.order_number += shiftChange;
      if (clone.group_order !== undefined) clone.group_order += shiftChange;
      if (clone.day_order !== undefined) clone.day_order += shiftChange;
    }

    newList.push(clone);
  }
}

function addMovedItem(
  oldList: Orderable[],
  newList: Orderable[],
  sourceIndex: number,
  newOrderNumber: number,
) {
  const clone = cloneDeep(oldList[sourceIndex]);

  if (clone.order_number !== undefined) clone.order_number = newOrderNumber;
  if (clone.group_order !== undefined) clone.group_order = newOrderNumber;
  if (clone.day_order !== undefined) clone.day_order = newOrderNumber;

  newList.push(clone);
}

function reorder(oldList: Orderable[], sourceIndex: number, destinationIndex: number) {
  const newList: Orderable[] = [];
  const lowIndex = sourceIndex < destinationIndex ? sourceIndex : destinationIndex;
  const highIndex = sourceIndex < destinationIndex ? destinationIndex : sourceIndex;
  const newOrderNumber = destinationIndex + 1; // Indexes start at 0.

  cloneFromUpTo(oldList, newList, 0, lowIndex);

  // If item is moved down.
  if (sourceIndex < destinationIndex) {
    // Shift items up. Skip source index since it's the item that is being moved. Add 1 to
    // destinationIndex since cloneFromUpTo excludes the item at the 2nd parameter index.
    cloneFromUpTo(oldList, newList, sourceIndex + 1, destinationIndex + 1, -1);

    addMovedItem(oldList, newList, sourceIndex, newOrderNumber);
  }

  // If item is moved up.
  if (destinationIndex < sourceIndex) {
    addMovedItem(oldList, newList, sourceIndex, newOrderNumber);

    // Shift items down.
    cloneFromUpTo(oldList, newList, destinationIndex, sourceIndex, 1);
  }

  cloneFromUpTo(oldList, newList, highIndex + 1, oldList.length);

  return newList;
}

export default reorder;
