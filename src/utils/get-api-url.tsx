let apiURL: string;

export default function getApiUrl() {
  if (apiURL === undefined) {
    const url = process.env.REACT_APP_API_URL ?? '';
    if (url === '') {
      throw Error('REACT_APP_API_URL environment variable is not set.');
    }

    apiURL = url;
  }

  return apiURL;
}
