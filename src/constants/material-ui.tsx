export const ANCHOR_TYPE_ELEMENT = 'anchorEl';
export const ANCHOR_TYPE_POSITION = 'anchorPosition';
export const ANCHOR_TYPE_NONE = 'none';
