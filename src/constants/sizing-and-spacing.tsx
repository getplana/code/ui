const dragHandleIconWidth = 0.9;
const dragHandlePadding = 0.6;
export const DRAG_HANDLE = {
  ICON_WIDTH: dragHandleIconWidth,
  PADDING: dragHandlePadding,
  WIDTH: dragHandleIconWidth + (dragHandlePadding * 2),
  MARGIN_RIGHT: 0.8,
};

export const TASK_ITEM = {
  WIDTH: 64,
  PADDING: 0.8,
};

export const TASK_GROUP = {
  WIDTH: DRAG_HANDLE.WIDTH + DRAG_HANDLE.MARGIN_RIGHT + DRAG_HANDLE.WIDTH + TASK_ITEM.WIDTH,
  MARGIN_VERTICAL: 2.4,
  OVERLAY_PADDING: 0.8,
};

export const MENU = {
  HEIGHT: 25.6,
  ITEM_HEIGHT: 3.2,
};

Object.freeze(DRAG_HANDLE);
Object.freeze(TASK_ITEM);
Object.freeze(TASK_GROUP);
Object.freeze(MENU);
