// Grays

export const GRAY_1200 = '#111418';
export const GRAY_1100 = '#23282F';
export const GRAY_1000 = '#363D45';
export const GRAY_900 = '#49515A';
export const GRAY_800 = '#5E666E';
export const GRAY_700 = '#717A84';
export const GRAY_600 = '#888F96';
export const GRAY_500 = '#9EA3A9';
export const GRAY_400 = '#B3B8BC';
export const GRAY_300 = '#C8CCD0';
export const GRAY_250 = '#D3D6D9';
export const GRAY_200 = '#DDE0E3';
export const GRAY_150 = '#E9EBED';
export const GRAY_125 = '#EEF0F2';
export const GRAY_100 = '#F4F5F6';

// Reds

export const RED_600 = '#E53E3E';
export const RED_800 = '#C53030';

// HSL

export const BLACK_HSL = {
  hue: 0,
  saturation: 0,
  lightness: 0,
};
Object.freeze(BLACK_HSL);

export const LIGHT_GRAY_HSL = {
  hue: 0,
  saturation: 0,
  lightness: 36,
};
Object.freeze(LIGHT_GRAY_HSL);
