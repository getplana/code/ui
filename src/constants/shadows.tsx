export const MENU_ELEVATION = 4;

// Source: Todoist
export const DRAG_OVERLAY_SHADOW = '0rem 0.5rem 0.8rem rgba(0, 0, 0, 16%)';

// Source: Timestripe
export const TASK_DIALOG_SHADOW = '0rem 0.4rem 0.8rem rgb(0, 0, 0, 7%)';
