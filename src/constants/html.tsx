export const LIST_ITEM_CLASS = 'list-item';
export const MORE_BUTTON_CLASS = 'more-button';
export const DISABLE_MOUSE_FOCUS_OUTLINE_CLASS = 'disable-mouse-focus-outline';
