import axios from 'axios';
import cloneDeep from 'lodash.clonedeep';
import {
  createContext,
  Dispatch,
  MutableRefObject,
  ReactNode,
  SetStateAction,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';

import {
  DataSource,
  GoalUpdate,
  GoalModel,
  HSLModel,
} from '../types';
import getApiUrl from '../utils/get-api-url';
import reorder from '../utils/reorder';

// --- Types --- //

interface ContextInterface {
  customerID: number;
  activeGoals: GoalModel[];
  archivedGoals: GoalModel[];
  setActiveGoals: Dispatch<SetStateAction<GoalModel[]>>;
  setArchivedGoals: Dispatch<SetStateAction<GoalModel[]>>;
  fetchGoals: (isActive: boolean) => void;
  archivedGoalsFetched: MutableRefObject<boolean>;
}

type Props = {
  customerID: number;
  children: ReactNode;
};

// --- Provider --- //

const GoalsContext = createContext<ContextInterface | undefined>(undefined);

export function GoalsProvider(props: Props) {
  const {
    customerID,
    children,
  } = props;

  const [activeGoals, setActiveGoals] = useState<GoalModel[]>([]);
  const [archivedGoals, setArchivedGoals] = useState<GoalModel[]>([]);

  // Archived goals were previously fetched every time user opened archived goals tabs. However,
  // this was causing a newly archived goal to not be shown if a user archived a goal and then
  // quickly after opened the archived goals tab. This is probably due to the API not finshing
  // updating the database before allowing the UI to fetch the archived goals. A flag is used here
  // to ensure that the archived goals are only fetched once each time the user revists the task
  // page.
  const archivedGoalsFetched = useRef(false);

  const fetchGoals = useCallback(async (isActive: boolean) => {
    try {
      const response = await axios.get<GoalModel[]>(`${getApiUrl()}/goals`, {
        params: {
          customer_id: customerID,
          is_active: isActive,
        },
      });

      if (isActive) {
        setActiveGoals(response.data);
      } else {
        setArchivedGoals(response.data);
      }
    } catch (error) {
      // TODO: Display error snackbar.
    }
  }, [customerID]);

  useEffect(() => {
    void fetchGoals(true);
  }, [fetchGoals]);

  return (
    <GoalsContext.Provider
      value={{
        customerID,
        activeGoals,
        archivedGoals,
        setActiveGoals,
        setArchivedGoals,
        fetchGoals,
        archivedGoalsFetched,
      }}
    >
      {children}
    </GoalsContext.Provider>
  );
}

// --- Hook --- //

export function useGoals(isActive: boolean) {
  const context = useContext(GoalsContext);
  if (context === undefined) {
    throw Error('useGoals must be used within a GoalsProvider.');
  }

  const {
    customerID,
    activeGoals,
    archivedGoals,
    setActiveGoals,
    setArchivedGoals,
    fetchGoals,
    archivedGoalsFetched,
  } = context;

  // Fetch archived goals if haven't fetched them yet.
  useEffect(() => {
    if (!isActive && !archivedGoalsFetched.current) {
      fetchGoals(isActive);
      archivedGoalsFetched.current = true;
    }
  }, [isActive, archivedGoalsFetched, fetchGoals]);

  // --- Adding goal --- //

  const addGoal = async (text: string, colorID: number) => {
    try {
      const newGoal = {
        customer_id: customerID,
        text,
        color_id: colorID,
      };

      const response = await axios.post<GoalModel>(`${getApiUrl()}/goals`, newGoal);

      const createdGoal = response.data;
      const newActiveGoals = cloneDeep(activeGoals);
      newActiveGoals.push(createdGoal);
      setActiveGoals(newActiveGoals);
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  // --- Updating goal text --- //

  const updateGoalTextHelper = (
    goalID: number,
    text: string,
    goals: GoalModel[],
    setGoals: Dispatch<SetStateAction<GoalModel[]>>,
  ) => {
    const updateIndex = goals.findIndex((goal) => goal.id === goalID);
    const newGoals = cloneDeep(goals);

    newGoals[updateIndex].text = text;
    setGoals(newGoals);
  };

  const updateGoalText = async (goalID: number, text: string, source: DataSource) => {
    if (source === DataSource.Client) {
      if (isActive) {
        updateGoalTextHelper(goalID, text, activeGoals, setActiveGoals);
      } else {
        updateGoalTextHelper(goalID, text, archivedGoals, setArchivedGoals);
      }
    } else if (source === DataSource.Server) {
      try {
        const goalUpdate = { text };
        await axios.patch(`${getApiUrl()}/goals/${goalID}`, goalUpdate, {
          params: { time_zone: Intl.DateTimeFormat().resolvedOptions().timeZone },
        });
      } catch (error) {
        // TODO: Display error snackbar and reverse change to state.
      }
    }
  };

  // --- Updating goal --- //

  const updateGoalColor = async (
    goalID: number,
    colorID: number,
    colorHSL: HSLModel,
    goals: GoalModel[],
    setGoals: Dispatch<SetStateAction<GoalModel[]>>,
  ) => {
    const updateIndex = goals.findIndex((goal) => goal.id === goalID);
    const newGoals = cloneDeep(goals);
    newGoals[updateIndex].color = colorHSL;
    setGoals(newGoals);

    try {
      const goalUpdate = { color_id: colorID };
      await axios.patch(`${getApiUrl()}/goals/${goalID}`, goalUpdate, {
        params: { time_zone: Intl.DateTimeFormat().resolvedOptions().timeZone },
      });
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  const updateGoalState = async (
    goalID: number,
    newIsActive: boolean,
    currGoals: GoalModel[],
    setCurrGoals: Dispatch<SetStateAction<GoalModel[]>>,
    otherGoals: GoalModel[],
    setOtherGoals: Dispatch<SetStateAction<GoalModel[]>>,
  ) => {
    const updateIndex = currGoals.findIndex((goal) => goal.id === goalID);
    const newCurrGoals = cloneDeep(currGoals);
    const newOtherGoals = cloneDeep(otherGoals);

    // Remove goal to archive/unarchive.
    const removedGoal = newCurrGoals.splice(updateIndex, 1)[0];

    // Decrease order number by 1 for goals below archived/unarchived goal.
    for (let i = updateIndex; i < newCurrGoals.length; i += 1) {
      newCurrGoals[i].order_number -= 1;
    }

    if (newIsActive) {
      // - If unarchiving - //

      // Set order number for unarchived goal.
      removedGoal.order_number = newOtherGoals.length + 1;

      // Add unarchived goal to list of active goals.
      newOtherGoals.splice(newOtherGoals.length, 0, removedGoal);
    } else if (!newIsActive && archivedGoalsFetched.current) {
      // - If archiving & archived goals already fetched - //

      // Set order number for archived goal.
      removedGoal.order_number = 1;

      // Increase order number by 1 for all archived goals.
      for (let i = 0; i < newOtherGoals.length; i += 1) {
        newOtherGoals[i].order_number += 1;
      }

      // Add archived goal to list of archived goals.
      newOtherGoals.splice(0, 0, removedGoal);
    }

    // Update client.
    setCurrGoals(newCurrGoals);
    setOtherGoals(newOtherGoals);

    // Update server.
    try {
      const goalUpdate = { is_active: newIsActive };
      await axios.patch(`${getApiUrl()}/goals/${goalID}`, goalUpdate);
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  const updateGoal = async (goalID: number, update: GoalUpdate) => {
    const {
      colorID,
      colorHSL,
      isActive: newIsActive,
    } = update;

    if (colorID !== undefined && colorHSL !== undefined) {
      if (isActive) {
        await updateGoalColor(goalID, colorID, colorHSL, activeGoals, setActiveGoals);
      } else {
        await updateGoalColor(goalID, colorID, colorHSL, archivedGoals, setArchivedGoals);
      }
    }

    if (newIsActive !== undefined) {
      const isArchiving = !newIsActive;
      if (isArchiving) {
        await updateGoalState(
          goalID,
          newIsActive,
          activeGoals,
          setActiveGoals,
          archivedGoals,
          setArchivedGoals,
        );
      } else if (!isArchiving) {
        await updateGoalState(
          goalID,
          newIsActive,
          archivedGoals,
          setArchivedGoals,
          activeGoals,
          setActiveGoals,
        );
      }
    }
  };

  // --- Reordering goal --- //

  const reorderGoalsHelper = async (
    sourceIndex: number,
    destinationIndex: number,
    goals: GoalModel[],
    setGoals: Dispatch<SetStateAction<GoalModel[]>>,
  ) => {
    // Saves ID of goal to reorder before updating state.
    const goalID = goals[sourceIndex].id;

    const newGoals = reorder(goals, sourceIndex, destinationIndex);
    setGoals(newGoals as GoalModel[]);

    try {
      const goalUpdate = { order_number: destinationIndex + 1 };
      await axios.patch(`${getApiUrl()}/goals/${goalID}`, goalUpdate);
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  const reorderGoal = async (sourceIndex: number, destinationIndex: number) => {
    if (isActive) {
      await reorderGoalsHelper(sourceIndex, destinationIndex, activeGoals, setActiveGoals);
    } else {
      await reorderGoalsHelper(sourceIndex, destinationIndex, archivedGoals, setArchivedGoals);
    }
  };

  // --- Deleting goal --- //

  const deleteGoalHelper = async (
    goalID: number,
    goals: GoalModel[],
    setGoals: Dispatch<SetStateAction<GoalModel[]>>,
  ) => {
    const updateIndex = goals.findIndex((goal) => goal.id === goalID);
    const newGoals = cloneDeep(goals);
    newGoals.splice(updateIndex, 1);

    // Decrease order number of goals below deleted one.
    for (let i = updateIndex; i < newGoals.length; i += 1) {
      newGoals[i].order_number -= 1;
    }

    setGoals(newGoals);

    try {
      await axios.delete(`${getApiUrl()}/goals/${goalID}`);
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  const deleteGoal = async (goalID: number) => {
    if (isActive) {
      await deleteGoalHelper(goalID, activeGoals, setActiveGoals);
    } else {
      await deleteGoalHelper(goalID, archivedGoals, setArchivedGoals);
    }
  };

  return {
    goals: isActive ? activeGoals : archivedGoals,
    addGoal,
    updateGoal,
    updateGoalText,
    reorderGoal,
    deleteGoal,
  };
}
