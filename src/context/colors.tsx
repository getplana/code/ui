import axios from 'axios';
import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from 'react';

import { ColorModel } from '../types';
import getApiUrl from '../utils/get-api-url';

// --- Types --- //

interface ContextInterface {
  colors: ColorModel[];
}

type Props = {
  children: ReactNode;
};

// --- Provider --- //

const ColorsContext = createContext<ContextInterface | undefined>(undefined);

export function ColorsProvider(props: Props) {
  const { children } = props;

  const [colors, setColors] = useState<ColorModel[]>([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await axios.get(`${getApiUrl()}/colors`);
        setColors(response.data);
      } catch (error) {
        // TODO: Display error snackbar.
      }
    }

    void fetchData();
  }, []);

  return (
    <ColorsContext.Provider
      value={{ colors }}
    >
      {children}
    </ColorsContext.Provider>
  );
}

// --- Hook --- //

export function useColors() {
  const context = useContext(ColorsContext);
  if (context === undefined) {
    throw Error('useColors must be used within a ColorsProvider.');
  }

  return context.colors;
}
