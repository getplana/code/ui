import axios from 'axios';
import cloneDeep from 'lodash.clonedeep';
import {
  createContext,
  Dispatch,
  MutableRefObject,
  ReactNode,
  SetStateAction,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';

import useErrorSnackbar from '../hooks/use-error-snackbar';
import {
  DayModel,
  HSLModel,
  LogLevel,
  TaskUpdate,
  TaskGroupUpdate,
  TaskGroupModel,
  TaskGroupUpdateModel,
  TaskModel,
  TaskUpdateModel,
} from '../types';
import getGroupIndexFromID from '../utils/drag-and-drop/get-group-index-from-id';
import getGroupIndexFromTaskID from '../utils/drag-and-drop/get-group-index-from-task-id';
import getTaskIndexFromID from '../utils/drag-and-drop/get-task-index-from-id';
import getApiUrl from '../utils/get-api-url';
import log from '../utils/log';
import reorder from '../utils/reorder';

// --- Types --- //

interface ContextInterface {
  customerID: number;
  today: string;
  taskGroups: TaskGroupModel[];
  prevTaskGroups: MutableRefObject<TaskGroupModel[] | null>;
  draggedOverGroupID: number | null;
  setTaskGroups: Dispatch<SetStateAction<TaskGroupModel[]>>;
  setDraggedOverGroupID: Dispatch<SetStateAction<number | null>>;
  errorSnackbar: ReturnType<typeof useErrorSnackbar>;
}

type Props = {
  customerID: number;
  children: ReactNode;
};

// --- Provider --- //

const TasksContext = createContext<ContextInterface | undefined>(undefined);

export function TasksProvider(props: Props) {
  const today = '2021-03-21';
  const {
    customerID,
    children,
  } = props;

  const prevTaskGroups = useRef<TaskGroupModel[] | null>([]);
  const [taskGroups, setTaskGroups] = useState<TaskGroupModel[]>([]);
  const [draggedOverGroupID, setDraggedOverGroupID] = useState<number | null>(null);
  const errorSnackbar = useErrorSnackbar();

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await axios.get<DayModel[]>(`${getApiUrl()}/tasks`, {
          params: {
            customer_id: customerID,
            from_date: today,
            num_days_before: 1,
          },
        });

        setTaskGroups(response.data[0].task_groups);
      } catch (error) {
        // TODO: Display error snackbar.
      }
    }

    void fetchData();
  }, [customerID, today]);

  return (
    <TasksContext.Provider
      value={{
        customerID,
        today,
        taskGroups,
        prevTaskGroups,
        draggedOverGroupID,
        setTaskGroups,
        setDraggedOverGroupID,
        errorSnackbar,
      }}
    >
      {children}
    </TasksContext.Provider>
  );
}

// --- Hook --- //

export function useTasks() {
  const context = useContext(TasksContext);
  if (context === undefined) {
    throw Error('useTasks must be used within a TasksProvider.');
  }

  const {
    customerID,
    today,
    taskGroups,
    prevTaskGroups,
    draggedOverGroupID,
    setTaskGroups,
    setDraggedOverGroupID,
    errorSnackbar,
  } = context;

  // --- Adding task --- //

  const addTask = async (
    text: string,
    goalID: number,
    goalColor: HSLModel,
    isCompleted: boolean,
    groupID: number,
  ) => {
    const groupIndex = getGroupIndexFromID(taskGroups, groupID);
    if (groupIndex === undefined) {
      await log(LogLevel.Error, `Group ${groupID} for new task not found in task group list, so could not add new task.`);
      return;
    }

    const groupOrder = taskGroups[groupIndex].tasks.length + 1;
    const newTask = {
      id: -1,
      text,
      goal_id: goalID,
      color: goalColor,
      is_completed: isCompleted,
      group_id: groupID,
      group_order: groupOrder,
    };

    prevTaskGroups.current = cloneDeep(taskGroups);
    let newTaskGroups = cloneDeep(taskGroups);
    newTaskGroups[groupIndex].tasks.push(newTask);
    setTaskGroups(newTaskGroups);

    try {
      const body = {
        customer_id: customerID,
        date: today,
        group_id: groupID,
        group_order: groupOrder,
        text,
        goal_id: goalID,
        is_completed: isCompleted,
      };

      const response = await axios.post<TaskModel>(`${getApiUrl()}/tasks`, body);

      const createdTask = response.data;
      newTaskGroups = cloneDeep(newTaskGroups);
      const groupTasks = newTaskGroups[groupIndex].tasks;
      newTaskGroups[groupIndex].tasks[groupTasks.length - 1].id = createdTask.id;
      setTaskGroups(newTaskGroups);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        errorSnackbar.open();

        setTaskGroups(prevTaskGroups.current);
        prevTaskGroups.current = null;
      }
    }
  };

  // --- Updating task --- //

  const updateTask = async (taskID: number, groupID: number, update: TaskUpdate) => {
    const {
      text,
      isChecked,
      goalID,
      goalColor,
    } = update;

    const groupUpdateIndex = taskGroups.findIndex((group) => group.id === groupID);
    const taskGroup = taskGroups[groupUpdateIndex];
    const taskUpdateIndex = taskGroup.tasks.findIndex((task) => task.id === taskID);

    const newTaskGroups = cloneDeep(taskGroups);
    const taskUpdate = {} as TaskUpdateModel;

    if (text !== undefined) {
      newTaskGroups[groupUpdateIndex].tasks[taskUpdateIndex].text = text;
      taskUpdate.text = text;
    }

    if (isChecked !== undefined) {
      newTaskGroups[groupUpdateIndex].tasks[taskUpdateIndex].is_completed = isChecked;
      taskUpdate.is_completed = isChecked;
    }

    if (goalID !== undefined && goalColor !== undefined) {
      newTaskGroups[groupUpdateIndex].tasks[taskUpdateIndex].color = goalColor;
      newTaskGroups[groupUpdateIndex].tasks[taskUpdateIndex].goal_id = goalID;
      taskUpdate.goal_id = goalID;
    }

    setTaskGroups(newTaskGroups);

    try {
      await axios.patch(`${getApiUrl()}/tasks/${taskID}`, taskUpdate);
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  const updateTaskColorsClient = (goalID: number, goalColor: HSLModel) => {
    const newTaskGroups = cloneDeep(taskGroups);
    for (let i = 0; i < newTaskGroups.length; i += 1) {
      const currTasks = newTaskGroups[i].tasks;
      for (let j = 0; j < currTasks.length; j += 1) {
        if (currTasks[j].goal_id === goalID) {
          currTasks[j].color = goalColor;
        }
      }
    }

    setTaskGroups(newTaskGroups);
  };

  // --- Reordering task --- //

  // Moves task to top of new group if the task is dragged from above, or to bottom of new group if
  // the task is dragged from below.
  const moveTaskToNewGroup = async (
    activeID: number,
    overID: number,
    isDraggingDown?: boolean,
  ) => {
    const activeGroupIndex = getGroupIndexFromTaskID(taskGroups, activeID);
    if (activeGroupIndex === undefined) {
      await log(LogLevel.Error, `Group for dragged task ${activeID} not found in task group list on drag over, so could not drag task to new group.`);
      return;
    }

    const overGroupIndex = getGroupIndexFromTaskID(taskGroups, overID);
    if (overGroupIndex === undefined) {
      await log(LogLevel.Error, `Group for task ${overID} to reorder dragged task to not found in task group list on drag over, so could not drag task to new group.`);
      return;
    }

    const activeGroup = taskGroups[activeGroupIndex];
    const activeGroupTasks = activeGroup.tasks;
    const activeTaskIndex = getTaskIndexFromID(activeGroupTasks, activeID);
    if (activeTaskIndex === undefined) {
      await log(LogLevel.Error, `Dragged task ${activeID} not found in task group list on drag over, so could not drag task to new group.`);
      return;
    }

    if (activeGroupIndex === overGroupIndex) {
      return;
    }

    // Remove dragged task from old group.

    const filteredActiveGroupTasks = [...activeGroupTasks].filter((task) => task.id !== activeID);

    // Shift up group order for tasks below dragged task.

    const newActiveGroupTasks = filteredActiveGroupTasks.map((task, index) => {
      if (index < activeTaskIndex || task.group_order === undefined) {
        return task;
      }

      return {
        ...task,
        group_order: task.group_order - 1,
      };
    });

    const overGroup = taskGroups[overGroupIndex];
    const overGroupTasks = overGroup.tasks;
    let newOverGroupTasks = [...overGroupTasks];

    // Shift down group order for tasks in new group if dragging task down to top of new group.

    if (isDraggingDown) {
      newOverGroupTasks = newOverGroupTasks.map((task) => {
        if (task.group_order === undefined) {
          return task;
        }

        return {
          ...task,
          group_order: task.group_order + 1,
        };
      });
    }

    // Add dragged task to new group.

    const movedTask = activeGroupTasks[activeTaskIndex];
    const newMovedTask = {
      ...movedTask,
      group_id: overGroup.id,
      group_order: isDraggingDown ? 1 : overGroupTasks.length + 1,
    };

    newOverGroupTasks = isDraggingDown ? [
      newMovedTask,
      ...overGroupTasks,
    ] : [
      ...overGroupTasks,
      newMovedTask,
    ];

    // Update state.

    const newActiveGroup = {
      ...activeGroup,
      tasks: newActiveGroupTasks,
    };

    const newOverGroup = {
      ...overGroup,
      tasks: newOverGroupTasks,
    };

    const newTaskGroups = [...taskGroups];
    newTaskGroups[activeGroupIndex] = newActiveGroup;
    newTaskGroups[overGroupIndex] = newOverGroup;

    setTaskGroups(newTaskGroups);
    setDraggedOverGroupID(taskGroups[overGroupIndex].id);

    // TODO: Delete

    // const newTaskGroups = cloneDeep(taskGroups);
    // const activeGroupTasks = newTaskGroups[activeGroupIndex].tasks;
    // const overGroupTasks = newTaskGroups[overGroupIndex].tasks;

    // // Shift up group order for tasks below dragged task.
    // for (let i = activeTaskIndex + 1; i < activeGroupTasks.length; i += 1) {
    //   const task = activeGroupTasks[i];
    //   if (task.group_order !== undefined) {
    //     task.group_order -= 1;
    //   }
    // }

    // // Remove dragged task from old group.
    // newTaskGroups[activeGroupIndex].tasks = activeGroupTasks.filter(
    //   (task) => task.id !== activeID,
    // );

    // // Shift down group order for tasks in new group if dragging task down to top of new group.
    // if (isDraggingDown) {
    //   for (let i = 0; i < overGroupTasks.length; i += 1) {
    //     const task = overGroupTasks[i];
    //     if (task.group_order !== undefined) {
    //       task.group_order += 1;
    //     }
    //   }
    // }

    // // Add dragged task to new group.
    // const movedTask = activeGroupTasks[activeTaskIndex];
    // movedTask.group_id = newTaskGroups[overGroupIndex].id;
    // movedTask.group_order = isDraggingDown ? 1 : overGroupTasks.length + 1;
    // newTaskGroups[overGroupIndex].tasks = isDraggingDown ? [
    //   movedTask,
    //   ...overGroupTasks,
    // ] : [
    //   ...overGroupTasks,
    //   movedTask,
    // ];

    // setTaskGroups(newTaskGroups);
    // setDraggedOverGroupID(taskGroups[overGroupIndex].id);
  };

  // Moves task to new order position, and makes API request to update group and order of task.
  const moveTaskToNewOrder = async (activeID: number, overID: number) => {
    const savedNewDndGroupID = draggedOverGroupID;
    setDraggedOverGroupID(null);

    const groupIndex = getGroupIndexFromTaskID(taskGroups, activeID);
    if (groupIndex === undefined) {
      await log(LogLevel.Error, `Group for dragged task ${activeID} not found in task group list on drag end, so could not reorder dragged task.`);
      return;
    }

    const groupTasks = taskGroups[groupIndex].tasks;
    const activeTaskIndex = getTaskIndexFromID(groupTasks, activeID);
    if (activeTaskIndex === undefined) {
      await log(LogLevel.Error, `Dragged task ${activeID} not found in task group list on drag end, so could not reorder dragged task.`);
      return;
    }

    const overTaskIndex = getTaskIndexFromID(groupTasks, overID);
    if (overTaskIndex === undefined) {
      await log(LogLevel.Error, `Task ${activeID} to reorder dragged task to not found in task group list on drag end, so could not reorder dragged task.`);
      return;
    }

    const newTaskGroups = cloneDeep(taskGroups);
    if (activeTaskIndex !== overTaskIndex) {
      const newTasks = reorder(groupTasks, activeTaskIndex, overTaskIndex);
      newTaskGroups[groupIndex].tasks = newTasks as TaskModel[];
      setTaskGroups(newTaskGroups);
    }

    try {
      if (savedNewDndGroupID !== null || activeTaskIndex !== overTaskIndex) {
        const taskUpdate = {
          group_id: savedNewDndGroupID,
          group_order: overTaskIndex + 1,
        };
        await axios.patch<DayModel[]>(`${getApiUrl()}/tasks/${activeID}`, taskUpdate);
      }
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  // --- Deleting task --- //

  const deleteTaskClient = (taskID: number, groupID: number) => {
    const newPrevTaskGroups = cloneDeep(taskGroups);
    prevTaskGroups.current = newPrevTaskGroups;

    const groupIndex = taskGroups.findIndex((group) => group.id === groupID);
    const { tasks } = taskGroups[groupIndex];
    const taskIndex = tasks.findIndex((task) => task.id === taskID);
    const newTaskGroups = cloneDeep(taskGroups);
    newTaskGroups[groupIndex].tasks.splice(taskIndex, 1);
    setTaskGroups(newTaskGroups);
  };

  const undeleteTaskClient = async () => {
    if (prevTaskGroups.current !== null) {
      const newTaskGroups = cloneDeep(prevTaskGroups.current);
      setTaskGroups(newTaskGroups);
    }
  };

  const deleteTaskServer = async (taskID: number) => {
    try {
      prevTaskGroups.current = null;
      await axios.delete(`${getApiUrl()}/tasks/${taskID}`);
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  const deleteGoalTasksClient = (goalID: number) => {
    const newTaskGroups = cloneDeep(taskGroups);

    for (let i = 0; i < newTaskGroups.length; i += 1) {
      const currGroup = newTaskGroups[i];
      const currTasks = currGroup.tasks;
      const newTasks = [] as TaskModel[];

      for (let j = 0; j < currTasks.length; j += 1) {
        const currTask = currTasks[j];
        if (currTask.goal_id !== goalID) {
          const newTask = cloneDeep(currTask);
          newTasks.push(newTask);
        }
      }

      currGroup.tasks = newTasks;
    }

    setTaskGroups(newTaskGroups);
  };

  // --- Adding task group --- //

  const addTaskGroup = async (name: string) => {
    const dayOrder = taskGroups.length + 1;
    const newGroup: TaskGroupModel = {
      id: -1,
      name,
      day_order: dayOrder,
      tasks: [],
    };

    prevTaskGroups.current = cloneDeep(taskGroups);
    let newTaskGroups = cloneDeep(taskGroups);
    newTaskGroups.push(newGroup);
    setTaskGroups(newTaskGroups);

    try {
      const body = {
        customer_id: customerID,
        date: today,
        name,
        day_order: dayOrder,
      };

      const response = await axios.post<TaskGroupModel>(`${getApiUrl()}/task-groups`, body);

      const createdGroup = response.data;
      newTaskGroups = cloneDeep(newTaskGroups);
      newTaskGroups[dayOrder - 1].id = createdGroup.id;
      setTaskGroups(newTaskGroups);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        errorSnackbar.open();

        setTaskGroups(prevTaskGroups.current);
        prevTaskGroups.current = null;
      }
    }
  };

  // --- Updating task group --- //

  const updateTaskGroup = async (groupID: number, update: TaskGroupUpdate) => {
    const { name } = update;

    const newTaskGroups = cloneDeep(taskGroups);
    const taskGroupUpdate = {} as TaskGroupUpdateModel;

    const groupIndex = getGroupIndexFromID(taskGroups, groupID);
    if (groupIndex === undefined) {
      await log(LogLevel.Error, `Group ${groupID} for task to update not found in task group list, so could not update task.`);
      return;
    }

    if (name !== undefined) {
      newTaskGroups[groupIndex].name = name;
      taskGroupUpdate.name = name;
    }

    setTaskGroups(newTaskGroups);

    try {
      await axios.patch(`${getApiUrl()}/task-groups/${groupID}`, taskGroupUpdate);
    } catch (error) {
      // TODO: Display error snackbar and reverse change to state.
    }
  };

  // --- Reordering task group --- //

  const reorderTaskGroup = async (activeID: number, overID: number) => {
    const activeIndex = getGroupIndexFromID(taskGroups, activeID);
    if (activeIndex === undefined) {
      await log(LogLevel.Error, `Group ${activeID} to reorder not found in task group list, so could not reorder group.`);
      return;
    }

    const overIndex = getGroupIndexFromID(taskGroups, overID);
    if (overIndex === undefined) {
      await log(LogLevel.Error, `Group ${overID} to reorder dragged group to not found in task group list, so could not reorder dragged group.`);
      return;
    }

    if (activeIndex !== overIndex) {
      const cloned = cloneDeep(taskGroups);
      const newTaskGroups = reorder(cloned, activeIndex, overIndex);
      setTaskGroups(newTaskGroups as TaskGroupModel[]);

      try {
        const taskGroupUpdate = { day_order: overIndex + 1 };
        await axios.patch(`${getApiUrl()}/task-groups/${activeID}`, taskGroupUpdate);
      } catch (error) {
        // TODO: Display error snackbar and reverse change to state.
      }
    }
  };

  return {
    taskGroups,
    addTask,
    updateTask,
    updateTaskColorsClient,
    moveTaskToNewGroup,
    moveTaskToNewOrder,
    deleteTaskClient,
    undeleteTaskClient,
    deleteTaskServer,
    deleteGoalTasksClient,
    addTaskGroup,
    updateTaskGroup,
    reorderTaskGroup,
  };
}
