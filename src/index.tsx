import { ChakraProvider } from '@chakra-ui/react';
import { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter,
  Route,
  Routes,
} from 'react-router-dom';

import App from './App';
import TaskPage from './components/task-page';

import './styles/index.scss';

ReactDOM.render(
  <StrictMode>
    <ChakraProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App />}>
            {/* 'index' used to set default route.
                  Source: https://stackoverflow.com/questions/70437952/is-there-a-way-to-set-a-default-route-with-react-router-v6 */}
            <Route index element={<TaskPage />} />
            <Route path="/tomorrow" element={<div>Tomorrow Page</div>} />
            <Route path="/timeline" element={<div>Timeline Page</div>} />
          </Route>
        </Routes>
      </BrowserRouter>
    </ChakraProvider>
  </StrictMode>,
  document.getElementById('root'),
);
