import {
  Icon,
  Text,
  useToast as useSnackbar,
} from '@chakra-ui/react';

import { ReactComponent as AlertIcon } from '../assets/alert.svg';
import Snackbar from '../components/snackbar';

export default function useErrorSnackbar() {
  const snackbar = useSnackbar();
  return {
    ...snackbar,
    open: () => {
      snackbar({
        position: 'bottom',
        duration: 3000,
        render: () => (
          <Snackbar
            onClose={() => snackbar.closeAll()}
          >
            <Icon
              as={AlertIcon}
              boxSize="1.6rem"
            />
            <Text
              fontSize="1.4rem"
              marginLeft="0.8rem"
            >
              An error occurred. Please try again.
            </Text>
          </Snackbar>
        ),
      });
    },
  };
}
