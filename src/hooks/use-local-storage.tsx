import { useState } from 'react';

export default function useLocalStorage<Type>(
  key: string,
  defaultValue: Type,
): [Type, (newValue: Type) => void] {
  const [value, setValue] = useState(() => {
    try {
      const currValue = window.localStorage.getItem(key);
      if (currValue !== null) {
        return JSON.parse(currValue) as Type;
      }

      window.localStorage.setItem(key, JSON.stringify(value));
      return defaultValue;
    } catch (error) {
      return defaultValue;
    }
  });

  const setStoredValue = (newValue: Type) => {
    try {
      window.localStorage.setItem(key, JSON.stringify(newValue));
      setValue(newValue);
    } catch (error) {
      setValue(newValue);
    }
  };

  return [value, setStoredValue];
}
