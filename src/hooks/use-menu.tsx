import {
  PopoverReference,
  PopoverPosition,
} from '@material-ui/core';
import {
  useCallback,
  useMemo,
  useState,
} from 'react';

import * as constants from '../constants';

export default function useMenu() {
  const [
    anchorType,
    setAnchorType,
  ] = useState<PopoverReference>(constants.ANCHOR_TYPE_NONE);
  const [anchorElement, setAnchorElement] = useState<HTMLElement | null>(null);
  const [anchorPosition, setAnchorPosition] = useState<PopoverPosition | null>(null);

  const openAtElement = useCallback((element: HTMLElement) => {
    setAnchorType(constants.ANCHOR_TYPE_ELEMENT);
    setAnchorElement(element);
    setAnchorPosition(null);
  }, []);

  const openAtPosition = useCallback((position: PopoverPosition) => {
    setAnchorType(constants.ANCHOR_TYPE_POSITION);
    setAnchorElement(null);
    setAnchorPosition(position);
  }, []);

  const close = useCallback(() => {
    setAnchorType(constants.ANCHOR_TYPE_NONE);
    setAnchorElement(null);
    setAnchorPosition(null);
  }, []);

  const actionMenu = useMemo(() => ({
    anchorType,
    anchorElement,
    anchorPosition,
    openAtElement,
    openAtPosition,
    close,
  }), [
    anchorType,
    anchorElement,
    anchorPosition,
    openAtElement,
    openAtPosition,
    close,
  ]);

  return actionMenu;
}
