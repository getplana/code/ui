// Copied from https://github.com/chakra-ui/chakra-ui/blob/main/.storybook/preview.tsx.

import { ChakraProvider } from "@chakra-ui/react"

const withChakra = (StoryFn) => {
  return (
    <ChakraProvider>
      <StoryFn />
    </ChakraProvider>
  )
}

export const decorators = [withChakra]